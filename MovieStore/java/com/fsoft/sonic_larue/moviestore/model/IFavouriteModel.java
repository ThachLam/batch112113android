package com.fsoft.sonic_larue.moviestore.model;

import com.fsoft.sonic_larue.moviestore.listener.OnGetMoviesFinishListener;

/**
 * Created by DungHT8 on 2015/12/08.
 */
public interface IFavouriteModel {
     void fetchData(String filter, OnGetMoviesFinishListener onGetMoviesFinishListener);

     void searchData(String filter, OnGetMoviesFinishListener onGetMoviesFinishListener);
}

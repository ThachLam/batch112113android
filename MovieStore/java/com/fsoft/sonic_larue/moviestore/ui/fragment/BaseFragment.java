package com.fsoft.sonic_larue.moviestore.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fsoft.sonic_larue.moviestore.R;
import com.fsoft.sonic_larue.moviestore.database.DatabaseHelper;
import com.fsoft.sonic_larue.moviestore.ui.MainActivity;
import com.fsoft.sonic_larue.moviestore.util.Util;

/**
 * Created by DungHT8 on 2015/11/23.
 */
public class BaseFragment extends Fragment {
    public MainActivity activity;
    public DatabaseHelper databaseHelper;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
        databaseHelper = new DatabaseHelper(activity);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navi_drawer, container, false);
        return view;
    }

    public Fragment getInstance(BaseFragment fragment, String tag) {
        fragment = (BaseFragment) getFragmentManager().findFragmentByTag(tag);
        if (fragment != null) {

        } else {

        }
        return fragment;
    }

//    public void fetchData() {
//
//    }

    public interface OnDetailReceiver {
        void onDetailReceiveFavourite(int position);

        void onDetailReceiveUpdateReminder(int movieId,String date);

        void onDetailReceiveDeleteReminder();
    }

    public interface OnDetailSender {
        void onDetailSendFavourite(int movieId, boolean isFavourite);

        void onDetailSendReminder(int movieId,String date);
    }
//    public interface OnChangeDataMovie {
//        void changeFavouriteFromMovie(int movieId, boolean isFavourite);
//        void changeReminderFromMovie(int position);
//    }

    public interface OnMovieSender {
        void onMovieUpdateFavourite(int movieId, boolean isFavourite);

        void onMovieUpdateReminder(int movieId,String date);
    }

    //    public interface OnChangeDataFavourite{
//        void changeFavouriteFromFavourite(int movieId, boolean isFavourite);
//        void changeReminderFromFavourite(int position);
//    }
    public interface OnFavouriteSender {
        void onFavouriteUpdateFavourite(int movieId, boolean isFavourite);

        void onFavouriteUpdateReminder(int movieId,String date);

    }


//    public interface OnChangeDataReminder {
//        void changeFavouriteFromMovie(int movieId, boolean isFavourite);
//
//        void changeReminderFromMovie(int position);
//    }
//interface OnChangeReminder {
//    void onReminder();
//
//    void onFavourite(int movieId, boolean isFavourite);
//}

    public interface OnReminderSender {
        void onReminderUpdateFavourite(int movieId, boolean isFavourite);

        void onReminderUpdateReminder(int movieId,String date);

        void onReminderDeleteReminder();
    }

//    public interface OnChangeReminder {
//        void onDeleteReminder();
//
//        void onChangeFavourite(int movieId, boolean isFavourite);
//    }

    public interface OnSettingSender {
        void onSettingUpdateFavourite(int movieId, boolean isFavourite);

        void onSettingUpdateReminder(int movieId,String date);

        void onSettingDeleteReminder();
    }



//    public interface OnPassDataSetting {
//        void changePassFromSetting(int position);
//
//        void changeFavouriteFromSetting(int movieId, boolean isFavourite);
//    }

    public interface OnReminderReceiver {
        void onReminderReceiveUpdateFavourite(int movieId, boolean isFavourite);

        void onReminderReceiveUpdateReminder(int movieId,String date);

        void onReminderReceiveDeleteReminder();
    }
}

package com.fsoft.sonic_larue.moviestore.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by DungHT8 on 2015/12/01.
 */
public class DateTime {
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static String toString(Date date) {
        return simpleDateFormat.format(date);
    }

    public static Date toDate(String str)  {
        Date date = null;
        try {
            date = simpleDateFormat.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}

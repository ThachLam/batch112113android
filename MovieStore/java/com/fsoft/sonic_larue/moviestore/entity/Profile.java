package com.fsoft.sonic_larue.moviestore.entity;

/**
 * Created by DungHT8 on 2015/12/01.
 */
public class Profile {
    public static final String  ID = "id";
    public static final String  NAME = "name";
    public static final String  EMAIL = "email";
    public static final String  BIRTH_DAY = "birth";
    public static final String  GENDER = "gender";
    public static final String  IMAGE = "image";
    public static final String  TABLE_PROFILE = "profile";


    private int id;
    private String name;
    private String email;
    private String birth;
    private int gender;
    private byte[] bitmap;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public byte[] getBitmap() {
        return bitmap;
    }

    public void setBitmap(byte[] bitmap) {
        this.bitmap = bitmap;
    }
}

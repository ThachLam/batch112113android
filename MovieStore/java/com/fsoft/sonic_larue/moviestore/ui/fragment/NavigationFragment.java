package com.fsoft.sonic_larue.moviestore.ui.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fsoft.sonic_larue.moviestore.R;
import com.fsoft.sonic_larue.moviestore.adapter.ReminderAdapter;
import com.fsoft.sonic_larue.moviestore.database.DatabaseHelper;
import com.fsoft.sonic_larue.moviestore.entity.Movie;
import com.fsoft.sonic_larue.moviestore.entity.Profile;
import com.fsoft.sonic_larue.moviestore.entity.Reminder;
import com.fsoft.sonic_larue.moviestore.model.NavigationModel;
import com.fsoft.sonic_larue.moviestore.present.INavigationPresent;
import com.fsoft.sonic_larue.moviestore.present.NavigationPresent;
import com.fsoft.sonic_larue.moviestore.ui.CircleImageView;
import com.fsoft.sonic_larue.moviestore.ui.MainActivity;
import com.fsoft.sonic_larue.moviestore.util.Util;
import com.fsoft.sonic_larue.moviestore.view.NavigationView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by DungHT8 on 2015/11/23.
 */
public class NavigationFragment extends Fragment implements View.OnClickListener, NavigationView {


    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private View containerView;

    private List<Reminder> reminders;


    private FragmentDrawerListener drawerListener;

    private Button btnShowAll;
    private Button btnEdit;
    private MainActivity activity;

    private TextView email;
    private TextView birth;
    private TextView gender;
    private TextView name;


    private TextView title;
    private TextView title1;

    private TextView date;
    private TextView date2;
    private TextView emptyData;


    private CircleImageView avatar;

    LinearLayout layoutContainer;
    INavigationPresent navigationPresent;
    CardView layoutReminder1;
    CardView layoutReminder2;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        navigationPresent = new NavigationPresent(this, getContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_navi_drawer, container, false);
        layout.setClickable(true);
        btnShowAll = (Button) layout.findViewById(R.id.show_all);
        btnEdit = (Button) layout.findViewById(R.id.edit);
        btnShowAll.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        layoutContainer = (LinearLayout) layout.findViewById(R.id.layout_container_reminder);

        //init avatar
        avatar = (CircleImageView) layout.findViewById(R.id.avatar);

        reminders = new ArrayList<>();


        activity = (MainActivity) getActivity();

        email = (TextView) layout.findViewById(R.id.email);
        birth = (TextView) layout.findViewById(R.id.birth_day);
        gender = (TextView) layout.findViewById(R.id.gender);
        name = (TextView) layout.findViewById(R.id.name);
        emptyData = (TextView) layout.findViewById(R.id.empty);

        layoutReminder1 = (CardView)layout.findViewById(R.id.reminder1);
        layoutReminder2 = (CardView)layout.findViewById(R.id.reminder2);

        title = (TextView) layout.findViewById(R.id.title);
        title1 = (TextView) layout.findViewById(R.id.title2);
        date = (TextView) layout.findViewById(R.id.date);
        date2 = (TextView) layout.findViewById(R.id.date2);

        navigationPresent.fetchProfile(1);
        return layout;
    }


    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        avatar.getLayoutParams().width = (int) (Util.getSizeScreen(activity).y * 1 / 5);
        avatar.getLayoutParams().height = (int) (Util.getSizeScreen(activity).y * 1 / 5);

        layoutContainer.getLayoutParams().height = (int) (2 * Util.getSizeScreen(activity).x / 6 + 5 * getResources().getDimension(R.dimen.margin_normal));

    }

    @Override
    public void onResume() {
        super.onResume();
        navigationPresent.fetchReminder();

    }

    public void setDrawerListener(FragmentDrawerListener listener) {
        this.drawerListener = listener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.show_all:
                drawerListener.onShowAllReminder();
                mDrawerLayout.closeDrawer(containerView);
                break;
            case R.id.edit:

                drawerListener.onEditProfile();
                break;
        }
    }

    public void close() {
        mDrawerLayout.closeDrawer(containerView);
    }

    public boolean isOpen() {
        return mDrawerLayout.isDrawerOpen(containerView);
    }

    @Override
    public void setDataReminder(List<Reminder> reminders) {
        this.reminders.clear();
        this.reminders.addAll(reminders);
        layoutReminder1.setVisibility(View.INVISIBLE);
        layoutReminder2.setVisibility(View.INVISIBLE);
        if (reminders.size() > 0) {
            for (int i = 0; i < (reminders.size() > 1 ? 2 : 1); i++) {
                Reminder current = reminders.get(i);
                if (i == 0 ) {
                    title.setText(current.getTitle() + " - " + current.getYear() + " - " + current.getRate() + "/10.0");
                    date.setText(current.getDate());
                    layoutReminder1.setVisibility(View.VISIBLE);
                }
                else if(i == 1) {
                    title1.setText(current.getTitle() + " - " + current.getYear() + " - " + current.getRate() + "/10.0");
                    date2.setText(current.getDate());
                    layoutReminder2.setVisibility(View.VISIBLE);
                }
            }
            emptyData.setVisibility(View.GONE);
        } else {
            emptyData.setVisibility(View.VISIBLE);
            layoutReminder1.setVisibility(View.INVISIBLE);
            layoutReminder2.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void setDataProfile(Profile profile) {
        email.setText(profile.getEmail());
        birth.setText(profile.getBirth());
        int genderText = profile.getGender();
        if (genderText == 1) {
            gender.setText(getResources().getString(R.string.male));
        } else {
            gender.setText(getResources().getString(R.string.female));
        }
        name.setText(profile.getName());

        if (profile.getBitmap() != null) {
            avatar.setImageBitmap(Util.getImage(profile.getBitmap()));
        }
    }


    public INavigationPresent getNavigationPresent() {
        return navigationPresent;
    }

    public interface FragmentDrawerListener {

        void onShowAllReminder();

        void onEditProfile();
    }
}

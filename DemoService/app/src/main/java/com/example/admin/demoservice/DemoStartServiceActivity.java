package com.example.admin.demoservice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class DemoStartServiceActivity extends AppCompatActivity {

    static final String TAG = DemoStartServiceActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo_start_service);
    }

    public void start(View view) {
        Intent i = new Intent(this, IntentServiceDemo.class);
//        Intent i = new Intent(this, StartServiceDemo.class);
        Log.d(TAG, "Start, main: "+ Thread.currentThread().hashCode());

        startService(i);
    }

    public void stop(View view) {
        Log.d(TAG, "Stop, main: "+ Thread.currentThread().hashCode());
        Intent i = new Intent(this, IntentServiceDemo.class);
//        Intent i = new Intent(this, StartServiceDemo.class);
        stopService(i);
    }
}

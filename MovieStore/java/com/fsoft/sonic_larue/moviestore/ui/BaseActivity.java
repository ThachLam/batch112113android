package com.fsoft.sonic_larue.moviestore.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;

import com.fsoft.sonic_larue.moviestore.ui.dialog.NetworkDialog;
import com.fsoft.sonic_larue.moviestore.util.Util;

/**
 * Created by DungHT8 on 2015/11/23.
 */
public class BaseActivity extends AppCompatActivity {
    static final String ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    NetworkDialog networkDialog = new NetworkDialog();

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(ACTION);
        this.registerReceiver(connectivityChangeReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.unregisterReceiver(connectivityChangeReceiver);
    }

    private final BroadcastReceiver connectivityChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!Util.checkInternet(context) && !networkDialog.isVisible()) {
                if(networkDialog.isAdded())
                {
                    return; //or return false/true, based on where you are calling from
                }
                networkDialog.show(getSupportFragmentManager(), "TAG");
            }
        }
    };
}

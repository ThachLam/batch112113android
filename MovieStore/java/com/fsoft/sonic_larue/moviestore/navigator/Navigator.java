package com.fsoft.sonic_larue.moviestore.navigator;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.fsoft.sonic_larue.moviestore.R;
import com.fsoft.sonic_larue.moviestore.entity.Reminder;
import com.fsoft.sonic_larue.moviestore.ui.fragment.DetailMovieFragment;
import com.fsoft.sonic_larue.moviestore.ui.fragment.FavouriteFragment;
import com.fsoft.sonic_larue.moviestore.ui.fragment.MovieFragment;
import com.fsoft.sonic_larue.moviestore.ui.fragment.PreferencesFragment;
import com.fsoft.sonic_larue.moviestore.ui.fragment.ProfileFragment;
import com.fsoft.sonic_larue.moviestore.ui.fragment.ReminderFragment;
import com.fsoft.sonic_larue.moviestore.ui.fragment.SettingFragment;
import com.fsoft.sonic_larue.moviestore.util.ActionEvent;
import com.fsoft.sonic_larue.moviestore.util.ActionEventConstant;

/**
 * Created by DungHT8 on 2015/12/09.
 */
public class Navigator implements INavigator {
    private static Navigator navigator;

    public static Navigator getInstance() {
        if (navigator == null) {
            return new Navigator();
        }
        return navigator;
    }

    @Override
    public void handleSwitchFragment(ActionEvent actionEvent) {
        switch (actionEvent.tag) {
            case ActionEventConstant.DETAIL_MOVIE_FRAGMENT:
                gotoDetailFragment(actionEvent);
                break;
            case ActionEventConstant.PROFILE_FRAGMENT:
                gotoEditProfileFragment(actionEvent);
                break;
            case ActionEventConstant.REMINDER_FRAGMENT:
                gotoReminderFragment(actionEvent);
                break;
            case ActionEventConstant.PREFERENCE_FRAGMENT:
                gotoPreferenceFragment(actionEvent);
                break;
        }
    }

    private void gotoPreferenceFragment(ActionEvent actionEvent) {
        FragmentManager fm = actionEvent.parentFragment.getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        PreferencesFragment preferencesFragment = null;
        preferencesFragment = (PreferencesFragment) fm.findFragmentByTag(PreferencesFragment.TAG);

        if (preferencesFragment != null) {
            ft.detach(preferencesFragment);
        }
        preferencesFragment = new PreferencesFragment();

        ft.replace(R.id.container_setting, preferencesFragment, PreferencesFragment.TAG);
        ft.addToBackStack(PreferencesFragment.TAG);
        ft.commit();
        fm.executePendingTransactions();
    }

    private void gotoReminderFragment(ActionEvent actionEvent) {
        FragmentManager fm = actionEvent.parentFragment.getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ReminderFragment reminderFragment = null;
        reminderFragment = (ReminderFragment) fm.findFragmentByTag(ReminderFragment.TAG);

        if (reminderFragment != null) {
            ft.remove(reminderFragment);
            fm.popBackStack();
        }
        reminderFragment = new ReminderFragment();

        ft.add(R.id.container_setting, reminderFragment, ReminderFragment.TAG);

        if (actionEvent.parentFragment instanceof SettingFragment) {
            SettingFragment settingFragment = (SettingFragment) actionEvent.parentFragment;
            settingFragment.setOnReminderReceiver(reminderFragment);
        }

        ft.addToBackStack(ReminderFragment.TAG);
        ft.commit();
        fm.executePendingTransactions();
    }

    private void gotoEditProfileFragment(ActionEvent actionEvent) {
        FragmentManager fm = actionEvent.fragmentActivity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_down, R.anim.slide_up, R.anim.slide_down, R.anim.slide_up);
        ProfileFragment profileFragment = (ProfileFragment) fm.findFragmentByTag(ProfileFragment.TAG);

        if (profileFragment != null) {
            ft.detach(profileFragment);
            ft.remove(profileFragment);
            fm.popBackStack();
        }
        profileFragment = new ProfileFragment();

        ft.add(R.id.container_main, profileFragment, ProfileFragment.TAG);
        ft.addToBackStack(ProfileFragment.TAG);
        ft.commit();
        fm.executePendingTransactions();
    }

    private void gotoDetailFragment(ActionEvent actionEvent) {
        FragmentManager fm = actionEvent.parentFragment.getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_down, R.anim.slide_up, R.anim.slide_down, R.anim.slide_up);
        DetailMovieFragment detailMovieFragment = (DetailMovieFragment) fm.findFragmentByTag(DetailMovieFragment.TAG);
        if (detailMovieFragment != null) {
            ft.detach(detailMovieFragment);
            ft.remove(detailMovieFragment);
        }

        detailMovieFragment = new DetailMovieFragment();
        detailMovieFragment.setArguments(actionEvent.bundle);

        //setListener
        if (actionEvent.parentFragment instanceof MovieFragment) {
            MovieFragment movieFragment = (MovieFragment) actionEvent.parentFragment;
            movieFragment.setOnDetailReceiver(detailMovieFragment);
        } else if (actionEvent.parentFragment instanceof FavouriteFragment) {
            FavouriteFragment favouriteFragment = (FavouriteFragment) actionEvent.parentFragment;
            favouriteFragment.setOnDetailReceiver(detailMovieFragment);

        } else if (actionEvent.parentFragment instanceof ReminderFragment) {
            ReminderFragment reminderFragment = (ReminderFragment) actionEvent.parentFragment;
            reminderFragment.setOnDetailReceiver(detailMovieFragment);
        }
        ft.add(R.id.container, detailMovieFragment, DetailMovieFragment.TAG);
        ft.addToBackStack(DetailMovieFragment.TAG);
        ft.commit();
        fm.executePendingTransactions();
    }
}

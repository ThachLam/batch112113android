package com.example.admin.handlerdemo;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

/**
 * Created by Admin on 8/24/2016.
 */
public class MainActivity extends Activity {

    String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 100; i++) {
                    Message msg = Message.obtain(null, 1, i, 0);
                    handler.sendMessage(msg);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 500; i < 600; i++) {
                    Message msg = Message.obtain(null, 2, i, 0);
                    Bundle b = new Bundle();
                    b.putString("msg", "Hello");
                    msg.setData(b);
                    handler.sendMessage(msg);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            String name = Thread.currentThread().getName();
            switch (msg.what) {
                case 1:
                    Log.d(TAG, "Source 1: " + name + ": "+ msg.arg1);
                    break;
                case 2:
                    Log.d(TAG, "Source 2: " + name + ": "+ msg.arg1);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    };
}

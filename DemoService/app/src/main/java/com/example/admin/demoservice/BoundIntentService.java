package com.example.admin.demoservice;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by Admin on 8/21/2016.
 */

public class BoundIntentService extends IntentService {

    static final String TAG = BoundIntentService.class.getSimpleName();

    private final IBinder mBinder = new LocalBinder();

    public BoundIntentService() {
        super("T");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        while (true) {
            Log.d("BoundService", "k = " + k++);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    class LocalBinder extends Binder {
        BoundIntentService getService () {
            return BoundIntentService.this;
        }
    }

    int k = 10;

    public void setK(int k) {
        this.k = k;
    }

    public int getK() {
        return k;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }
}

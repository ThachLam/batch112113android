package com.fsoft.sonic_larue.moviestore.util;

import android.text.TextUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by DungHT8 on 2015/12/04.
 */
public class Validate {
    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    public static boolean isValidBirth(String date) {
        Date date1 = DateTime.toDate(date);
        if (date1 != null) {
            if (date1.getTime() > Calendar.getInstance().getTime().getTime()) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean isValidReminder(Calendar calendar) {
        if (calendar.getTime().getTime() < Calendar.getInstance().getTime().getTime()) {
            return false;
        }
        return true;
    }
}

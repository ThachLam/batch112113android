package com.fsoft.sonic_larue.moviestore.model;

import android.os.Bundle;

import com.fsoft.sonic_larue.moviestore.entity.Movie;
import com.fsoft.sonic_larue.moviestore.listener.OnGetCastFinishListener;
import com.fsoft.sonic_larue.moviestore.listener.OnGetMovieFinishListener;
import com.fsoft.sonic_larue.moviestore.listener.OnGetDetailFinishListener;

import java.util.Calendar;

/**
 * Created by DungHT8 on 2015/12/08.
 */
public interface IDetailMovieModel {
    void fetchCast(OnGetDetailFinishListener onGetDetailFinishListener, int movieID);
    void fetchMovie(OnGetDetailFinishListener onGetDetailFinishListener, int movieID);
    void fetchMovie(OnGetDetailFinishListener onGetDetailFinishListener, Bundle bundle);

    void updateReminder(Movie movie, Calendar calendar, OnGetDetailFinishListener onUpdateFinishListener);
}

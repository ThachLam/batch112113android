package com.fsoft.sonic_larue.moviestore.view;

import com.fsoft.sonic_larue.moviestore.entity.Movie;

import java.util.List;

/**
 * Created by DungHT8 on 2015/12/07.
 */
public interface FavouriteView {
    void showProgress();

    void hideProgress();

    void refreshAdapter();

    void setDataFavourite(List<Movie> movies);
}

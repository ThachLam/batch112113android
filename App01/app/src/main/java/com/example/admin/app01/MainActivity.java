package com.example.admin.app01;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.FrameLayout;

/**
 * Created by Admin on 8/17/2016.
 */
public class MainActivity extends AppCompatActivity {

    boolean flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("MainActivity", "onCreate");

        flag = getResources().getBoolean(R.bool.mode);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (flag) {
            ListFragment listFragment = new ListFragment();
            ft.replace(R.id.frameList, listFragment, "List");
            ft.addToBackStack("List");
            ft.commit();
        } else {
            ListFragment listFragment = new ListFragment();
            DetailFragment detailFragment = new DetailFragment();
            ft.replace(R.id.frameList, listFragment, "List");
            ft.addToBackStack("List");
            ft.commit();
            ft = fm.beginTransaction();
            ft.replace(R.id.frameDetail, detailFragment, "Detail");
            ft.addToBackStack("Detail");
            ft.commit();
        }
    }

    public void showDetail(Contact contact) {
        FragmentManager fm = getSupportFragmentManager();
        if (!flag) {
            Fragment fragment = fm.findFragmentByTag("Detail");
            if (fragment != null) {
                DetailFragment detailFragment = (DetailFragment) fragment;
                detailFragment.setData(contact);
            }
        } else {
            FragmentTransaction ft = fm.beginTransaction();
            DetailFragment detailFragment = new DetailFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable("contact", contact);
            detailFragment.setArguments(bundle);

            ft.replace(R.id.frameList, detailFragment, "Detail");
            ft.addToBackStack("Detail");
            ft.commit();
        }
    }

//    public void clickButton(View view) {
//        boolean flag = getResources().getBoolean(R.bool.mode);
//        Log.d("MainActivity", flag + "");
//    }
}

package com.fsoft.sonic_larue.moviestore.model;

import android.content.Context;

import com.fsoft.sonic_larue.moviestore.database.DatabaseHelper;
import com.fsoft.sonic_larue.moviestore.entity.Reminder;
import com.fsoft.sonic_larue.moviestore.listener.OnGetReminderFinishListener;

import java.util.ArrayList;

/**
 * Created by DungHT8 on 2015/12/08.
 */
public class ReminderModel implements IReminderModel {
    private Context context;
    private DatabaseHelper databaseHelper;

    public ReminderModel(Context context) {
        this.context = context;
        this.databaseHelper = new DatabaseHelper(context);
    }

    @Override
    public void fetchReminder(OnGetReminderFinishListener reminderFinishListener) {
        ArrayList<Reminder> reminders = databaseHelper.getReminders();
        if (reminders != null) {
            reminderFinishListener.onSuccess(reminders);
        } else {
            reminderFinishListener.onError();
        }
    }
}

package com.example.admin.app01;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Admin on 8/10/2016.
 */
public class DemoListviewActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.horizontal_scroll_view);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        List<String> authorList = new ArrayList<>();
        for (char c = 'A'; c < 'Z'; c++) {
            authorList.add("NV" + c);
        }
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        for (final String name : authorList) {
            View view = inflater.inflate(R.layout.horizontal_item, null, false);
            TextView tvName = (TextView) view.findViewById(R.id.tvName);
            tvName.setText(name);
            linearLayout.addView(view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("Name", name);
                }
            });
        }

//        setContentView(R.layout.listview_demo);
//
//        ListView listView = (ListView) findViewById(R.id.listView);
//
//        List<Contact> contactList = new ArrayList<>();
//        for (char c = 'A'; c < 'Z'; c++) {
//            contactList.add(new Contact("NV" + c, "01111111111" + c));
//        }
//        ListAdapter listAdapter = new ListAdapter(this, contactList);
//        listView.setAdapter(listAdapter);

//        setContentView(R.layout.gridview_demo);
//
//        GridView listView = (GridView) findViewById(R.id.gridView);
//
//        List<Contact> contactList = new ArrayList<>();
//        for (char c = 'A'; c < 'Z'; c++) {
//            contactList.add(new Contact("NV" + c, "01111111111" + c));
//        }
//        GridAdapter gridAdapter = new GridAdapter(this, contactList);
//        listView.setAdapter(gridAdapter);
    }
}

package com.fsoft.sonic_larue.moviestore.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.fsoft.sonic_larue.moviestore.entity.Movie;
import com.fsoft.sonic_larue.moviestore.entity.Profile;
import com.fsoft.sonic_larue.moviestore.entity.Reminder;

import java.util.ArrayList;

/**
 * Created by DungHT8 on 2015/11/18.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "movie_store";
    public static final String TABLE_MOVIE = "movie";
    public static final String TABLE_REMINDER = "reminder";

    private static final String TAG = DatabaseHelper.class.getSimpleName();

    private static DatabaseHelper databaseHelper;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    public static synchronized DatabaseHelper getInstance(Context context) {
        if (databaseHelper == null) {
            return new DatabaseHelper(context.getApplicationContext());
        }
        return databaseHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTableMovie = "CREATE TABLE " + TABLE_MOVIE + " ("
                + Movie.ID_TABLE + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Movie.ID + " INTEGER, "
                + Movie.OVERVIEW + " TEXT, "
                + Movie.RELEASE_DATE + " INTEGER, "
                + Movie.TITLE + " TEXT, "
                + Movie.POSTER + " TEXT, "
                + Movie.VOTE_AVERAGE + " TEXT, "
                + Movie.ADULT + " INTEGER"
                + ")";

        String createTableReminder = "CREATE TABLE " + TABLE_REMINDER + " ("
                + Reminder.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Reminder.TITLE + " TEXT, "
                + Reminder.POSTER_PATH + " TEXT, "
                + Reminder.RATE + " TEXT, "
                + Reminder.YEAR + " TEXT, "
                + Reminder.DATE + " TEXT, "
                + Reminder.MOVIE_ID + " INTEGER "
                + ")";

        String createTableProfile = "CREATE TABLE " + Profile.TABLE_PROFILE + " ("
                + Profile.ID + " INTEGER PRIMARY KEY, "
                + Profile.NAME + " TEXT, "
                + Profile.EMAIL + " TEXT, "
                + Profile.BIRTH_DAY + " TEXT, "
                + Profile.GENDER + " INTEGER, "
                + Profile.IMAGE + " BLOB "
                + ")";
        db.execSQL(createTableMovie);
        db.execSQL(createTableReminder);
        db.execSQL(createTableProfile);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS +" + TABLE_MOVIE);
        db.execSQL("DROP TABLE IF EXISTS +" + TABLE_REMINDER);
        db.execSQL("DROP TABLE IF EXISTS +" + Profile.TABLE_PROFILE);
        onCreate(db);
    }

    public long insertMovie(Movie movie) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Movie.ID, movie.getId());
        values.put(Movie.OVERVIEW, movie.getOverview());
        values.put(Movie.RELEASE_DATE, movie.getReleaseDate());
        values.put(Movie.TITLE, movie.getTitle());
        values.put(Movie.POSTER, movie.getPoster());
        values.put(Movie.VOTE_AVERAGE, movie.getRating());
        int flag = 0;
        if (movie.isAdult()) {
            flag = 1;
        } else {
            flag = 0;
        }
        values.put(Movie.ADULT, flag);

        return database.insert(TABLE_MOVIE, null, values);
    }

    public long insertReminder(Reminder reminder) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Reminder.TITLE, reminder.getTitle());
        values.put(Reminder.POSTER_PATH, reminder.getPosterPath());
        values.put(Reminder.RATE, reminder.getRate());
        values.put(Reminder.YEAR, reminder.getYear());
        values.put(Reminder.DATE, reminder.getDate());
        values.put(Reminder.MOVIE_ID, reminder.getMovieId());

        Log.d(DatabaseHelper.TAG, "insert reminder");
        return database.insert(TABLE_REMINDER, null, values);
    }


    public long updateMovie(Movie movie, int id) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Movie.ID, movie.getId());
        values.put(Movie.OVERVIEW, movie.getOverview());
        values.put(Movie.RELEASE_DATE, movie.getReleaseDate());
        values.put(Movie.TITLE, movie.getTitle());
        values.put(Movie.POSTER, movie.getPoster());
        values.put(Movie.VOTE_AVERAGE, movie.getRating());
        int flag = 0;
        if (movie.isAdult()) {
            flag = 1;
        } else {
            flag = 0;
        }
        values.put(Movie.ADULT, flag);
        return database.update(TABLE_MOVIE, values, Movie.ID_TABLE + " = " + id, null);
    }


    public long updateReminder(Reminder reminder) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Reminder.TITLE, reminder.getTitle());
        values.put(Reminder.POSTER_PATH, reminder.getPosterPath());
        values.put(Reminder.RATE, reminder.getRate());
        values.put(Reminder.YEAR, reminder.getYear());
        values.put(Reminder.DATE, reminder.getDate());
        values.put(Reminder.MOVIE_ID, reminder.getMovieId());
        values.put(Reminder.ID, reminder.getId());

        Log.d(DatabaseHelper.TAG, "update reminder");
        return database.update(TABLE_REMINDER, values, Reminder.MOVIE_ID + " = " + reminder.getMovieId(), null);
    }

    public Movie getMovie(int id) {
        SQLiteDatabase database = this.getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_MOVIE + " WHERE " + Movie.ID + " = " + id;
        Log.d(TAG, sql);


        Cursor c = database.rawQuery(sql, null);
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
        } else {
            return null;
        }
        Movie movie = new Movie();
        movie.set_id(c.getInt(c.getColumnIndex(Movie.ID_TABLE)));
        movie.setId(c.getInt(c.getColumnIndex(Movie.ID)));
        movie.setOverview(c.getString(c.getColumnIndex(Movie.OVERVIEW)));
        movie.setReleaseDate(c.getString(c.getColumnIndex(Movie.RELEASE_DATE)));
        movie.setTitle(c.getString(c.getColumnIndex(Movie.TITLE)));
        movie.setPoster(c.getString(c.getColumnIndex(Movie.POSTER)));
        movie.setRating(c.getFloat(c.getColumnIndex(Movie.VOTE_AVERAGE)));
        movie.setAdult(c.getInt(c.getColumnIndex(Movie.ADULT)) == 1);
        c.close();
        return movie;
    }

    public Reminder getReminder(int movieId) {
        SQLiteDatabase database = this.getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_REMINDER + " WHERE " + Reminder.MOVIE_ID + " = " + movieId;
        Log.d(TAG, sql);


        Cursor c = database.rawQuery(sql, null);
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
        } else {
            return null;
        }
        Reminder reminder = new Reminder();
        reminder.setId(c.getInt(c.getColumnIndex(Reminder.ID)));
        reminder.setTitle(c.getString(c.getColumnIndex(Reminder.TITLE)));
        reminder.setPosterPath(c.getString(c.getColumnIndex(Reminder.POSTER_PATH)));
        reminder.setRate(c.getFloat(c.getColumnIndex(Reminder.RATE)));
        reminder.setYear(c.getString(c.getColumnIndex(Reminder.YEAR)));
        reminder.setDate(c.getString(c.getColumnIndex(Reminder.DATE)));
        reminder.setMovieId(c.getInt(c.getColumnIndex(Reminder.MOVIE_ID)));

        c.close();
        return reminder;
    }

    public ArrayList<Movie> getMovies() {

        SQLiteDatabase database = this.getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_MOVIE;
        Log.d(TAG, sql);


        Cursor c = database.rawQuery(sql, null);
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
        } else {
            return new ArrayList<>();
        }

        ArrayList<Movie> movies = new ArrayList<>();
        do {
            Movie movie = new Movie();
            movie.set_id(c.getInt(c.getColumnIndex(Movie.ID_TABLE)));
            movie.setId(c.getInt(c.getColumnIndex(Movie.ID)));
            movie.setOverview(c.getString(c.getColumnIndex(Movie.OVERVIEW)));
            movie.setReleaseDate(c.getString(c.getColumnIndex(Movie.RELEASE_DATE)));
            movie.setTitle(c.getString(c.getColumnIndex(Movie.TITLE)));
            movie.setPoster(c.getString(c.getColumnIndex(Movie.POSTER)));
            movie.setRating(c.getFloat(c.getColumnIndex(Movie.VOTE_AVERAGE)));
            movie.setAdult(c.getInt(c.getColumnIndex(Movie.ADULT)) == 1);

            movies.add(movie);
        } while (c.moveToNext());
        c.close();
        return movies;
    }

    public ArrayList<Movie> getMovies(String title) {

        SQLiteDatabase database = this.getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_MOVIE + " WHERE lower(" + Movie.TITLE + ") like '%" + title + "%'";
        Log.d(TAG, sql);


        Cursor c = database.rawQuery(sql, null);
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
        } else {
            return null;
        }

        ArrayList<Movie> movies = new ArrayList<>();
        do {
            Movie movie = new Movie();
            movie.set_id(c.getInt(c.getColumnIndex(Movie.ID_TABLE)));
            movie.setId(c.getInt(c.getColumnIndex(Movie.ID)));
            movie.setOverview(c.getString(c.getColumnIndex(Movie.OVERVIEW)));
            movie.setReleaseDate(c.getString(c.getColumnIndex(Movie.RELEASE_DATE)));
            movie.setTitle(c.getString(c.getColumnIndex(Movie.TITLE)));
            movie.setPoster(c.getString(c.getColumnIndex(Movie.POSTER)));
            movie.setRating(c.getFloat(c.getColumnIndex(Movie.VOTE_AVERAGE)));
            movie.setAdult(c.getInt(c.getColumnIndex(Movie.ADULT)) == 1);

            movies.add(movie);
        } while (c.moveToNext());
        c.close();
        return movies;
    }

    public ArrayList<Reminder> getReminders() {

        SQLiteDatabase database = this.getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_REMINDER;
        Log.d(TAG, sql);


        Cursor c = database.rawQuery(sql, null);
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
        } else {
            return new ArrayList<>();
        }

        ArrayList<Reminder> reminders = new ArrayList<>();
        do {
            Reminder reminder = new Reminder();
            reminder.setId(c.getInt(c.getColumnIndex(Reminder.ID)));
            reminder.setTitle(c.getString(c.getColumnIndex(Reminder.TITLE)));
            reminder.setPosterPath(c.getString(c.getColumnIndex(Reminder.POSTER_PATH)));
            reminder.setRate(c.getFloat(c.getColumnIndex(Reminder.RATE)));
            reminder.setYear(c.getString(c.getColumnIndex(Reminder.YEAR)));
            reminder.setDate(c.getString(c.getColumnIndex(Reminder.DATE)));
            reminder.setMovieId(c.getInt(c.getColumnIndex(Reminder.MOVIE_ID)));

            reminders.add(reminder);
        } while (c.moveToNext());
        c.close();
        return reminders;
    }

    //    public Cursor getBooksCursor() {
//
//        SQLiteDatabase database = this.getReadableDatabase();
//        String sql = "SELECT * FROM " + TABLE;
//        Log.d(TAG, sql);
//        Cursor c = database.rawQuery(sql, null);
//
//
//        return c;
//
//    }
//
    public long deleteMovie(int id) {
        SQLiteDatabase database = this.getWritableDatabase();
        return database.delete(TABLE_MOVIE, Movie.ID + " = " + id, null);
    }

    public long deleteReminder(int movieId) {
        SQLiteDatabase database = this.getWritableDatabase();
        return database.delete(TABLE_REMINDER, Reminder.MOVIE_ID + " = " + movieId, null);
    }

//    public long deleteBooks() {
//        SQLiteDatabase database = this.getWritableDatabase();
//        return database.delete(TABLE, null, null);
//    }null

    public Boolean isFavourite(int movieId) {
        SQLiteDatabase database = this.getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_MOVIE + " WHERE " + Movie.ID + " = " + movieId;
        Log.d(TAG, sql);

        Cursor c = database.rawQuery(sql, null);
        if (c != null && c.getCount() > 0) {
            c.close();
            return true;
        } else {
            c.close();
            return false;
        }
    }

    public Boolean isReminder(int movieId) {
        SQLiteDatabase database = this.getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_REMINDER + " WHERE " + Reminder.MOVIE_ID + " = " + movieId;
        Log.d(TAG, sql);

        Cursor c = database.rawQuery(sql, null);
        if (c != null && c.getCount() > 0) {
            c.close();
            return true;
        } else {
            c.close();
            return false;
        }
    }


    public long insertProfile(Profile profile) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Profile.ID, 1);
        values.put(Profile.NAME, profile.getName());
        values.put(Profile.EMAIL, profile.getEmail());
        values.put(Profile.BIRTH_DAY, profile.getBirth());
        values.put(Profile.GENDER, profile.getGender());
        if (profile != null) {
            values.put(Profile.IMAGE, profile.getBitmap());
        }

        Log.d(DatabaseHelper.TAG, "insert profile");
        return database.insert(Profile.TABLE_PROFILE, null, values);
    }


    public long updateProfile(Profile profile) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Profile.ID, 1);
        values.put(Profile.NAME, profile.getName());
        values.put(Profile.EMAIL, profile.getEmail());
        values.put(Profile.BIRTH_DAY, profile.getBirth());
        values.put(Profile.GENDER, profile.getGender());
        values.put(Profile.IMAGE, profile.getBitmap());
        Log.d(DatabaseHelper.TAG, "update profile ");
        return database.update(Profile.TABLE_PROFILE, values, Profile.ID + " = " + 1, null);
    }

    public Profile getProfile() {
        SQLiteDatabase database = this.getReadableDatabase();
        String sql = "SELECT * FROM " + Profile.TABLE_PROFILE + " WHERE " + Profile.ID + " = 1";


        Cursor c = database.rawQuery(sql, null);
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
        } else {
            c.close();
            return null;
        }
        Log.d(TAG, sql + "----" + c.getCount());

        Profile profile = new Profile();
        profile.setId(c.getInt(c.getColumnIndex(Profile.ID)));
        profile.setName(c.getString(c.getColumnIndex(Profile.NAME)));
        profile.setBirth(c.getString(c.getColumnIndex(Profile.BIRTH_DAY)));
        profile.setEmail(c.getString(c.getColumnIndex(Profile.EMAIL)));
        profile.setGender(c.getInt(c.getColumnIndex(Profile.GENDER)));
        byte[] image = c.getBlob(c.getColumnIndex(Profile.IMAGE));
        if (image != null) {
            profile.setBitmap(c.getBlob(c.getColumnIndex(Profile.IMAGE)));
        }
        c.close();
        return profile;
    }
}

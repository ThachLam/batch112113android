package com.fsoft.sonic_larue.moviestore.listener;

import com.fsoft.sonic_larue.moviestore.entity.Movie;
import com.fsoft.sonic_larue.moviestore.entity.Profile;
import com.fsoft.sonic_larue.moviestore.entity.Reminder;

import java.util.List;

/**
 * Created by DungHT8 on 2015/12/05.
 */
public interface OnGetReminderFinishListener {
    void onSuccess(List<Reminder> movies);
    void onError();

    void onSuccessGetProfile(Profile  profile);
}

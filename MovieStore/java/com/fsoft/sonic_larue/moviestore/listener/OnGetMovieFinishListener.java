package com.fsoft.sonic_larue.moviestore.listener;

import com.fsoft.sonic_larue.moviestore.entity.Movie;

import java.util.List;

/**
 * Created by DungHT8 on 2015/12/05.
 */
public interface OnGetMovieFinishListener {
    void onSuccess(Movie movie);
    void onError();
}

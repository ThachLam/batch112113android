package com.fsoft.sonic_larue.moviestore.listener;

import com.fsoft.sonic_larue.moviestore.entity.Cast;
import com.fsoft.sonic_larue.moviestore.entity.Movie;
import com.fsoft.sonic_larue.moviestore.entity.Reminder;

import java.util.List;

/**
 * Created by DungHT8 on 2015/12/05.
 */
public interface OnGetDetailFinishListener {
    void onSuccessUpdate(String date);
    void onErrorUpdate();

    void onSuccessCasts(List<Cast> casts);
    void onErrorCast();

    void onSuccessMovie(Movie movie);
    void onErrorMovie();
}

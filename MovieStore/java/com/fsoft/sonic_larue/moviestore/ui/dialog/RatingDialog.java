package com.fsoft.sonic_larue.moviestore.ui.dialog;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.fsoft.sonic_larue.moviestore.R;

import java.text.DecimalFormat;

/**
 * Created by DungHT8 on 2015/11/30.
 */
public class RatingDialog extends DialogFragment {
    OnDateSeekBar onDateSeekBar;
    float process;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            process = bundle.getFloat("process");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final SeekBar seekBar = new SeekBar(getActivity());
        final TextView tv = new TextView(getActivity());


        LinearLayout relativeLayout = new LinearLayout(getActivity());
        relativeLayout.setOrientation(LinearLayout.HORIZONTAL);
        relativeLayout.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, RelativeLayout.LayoutParams.WRAP_CONTENT);

        params.weight = 1;
        params.gravity = Gravity.CENTER;

        tv.setLayoutParams(params);
        tv.setTextColor(getResources().getColor(android.R.color.black));

        params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;
        params.weight = 5;
        seekBar.setLayoutParams(params);

        relativeLayout.addView(seekBar);
        relativeLayout.addView(tv);

        seekBar.setMax(100);
        seekBar.setProgress(toInteger(process));
        tv.setText(String.valueOf(process));
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //   tv.setText(progress);
                tv.setText(String.valueOf(toFloat(seekBar.getProgress())));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        String context = getResources().getString(R.string.movie_with_rate);
        String ok = getResources().getString(R.string.ok);
        String cancel = getResources().getString(R.string.cancel);
        return new AlertDialog.Builder(getActivity())
                // Set Dialog Message
                .setMessage(context)

                        // Positive button
                .setPositiveButton(cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Do something else
                    }
                })
                .setNegativeButton(ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onDateSeekBar.onReceive(toFloat(seekBar.getProgress()));
                    }
                })
                .setView(relativeLayout)
                .create();

    }

    public void setSeekBarListener(OnDateSeekBar seekBarListener) {
        this.onDateSeekBar = seekBarListener;
    }

    public interface OnDateSeekBar {
        void onReceive(float process);
    }

    public float toFloat(int intVal) {
        float floatVal = 0.0f;
        floatVal = .1f * intVal;
        return Float.valueOf(new DecimalFormat("##.##").format(floatVal));
    }

    public int toInteger(float intVal) {
        return (int) (intVal * 10);
    }
}

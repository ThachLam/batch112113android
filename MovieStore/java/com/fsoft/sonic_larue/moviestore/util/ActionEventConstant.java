package com.fsoft.sonic_larue.moviestore.util;

/**
 * Created by DungHT8 on 2015/12/09.
 */
public class ActionEventConstant {
    public static final int DETAIL_MOVIE_FRAGMENT = 100;
    public static final int PROFILE_FRAGMENT = 101;
    public static final int SETTING_FRAGMENT = 102;
    public static final int PREFERENCE_FRAGMENT = 103;
    public static final int REMINDER_FRAGMENT = 104;

}

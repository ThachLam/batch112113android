package com.fsoft.sonic_larue.moviestore.model;

import android.content.Context;

import com.fsoft.sonic_larue.moviestore.R;
import com.fsoft.sonic_larue.moviestore.database.DatabaseHelper;
import com.fsoft.sonic_larue.moviestore.entity.Profile;
import com.fsoft.sonic_larue.moviestore.entity.Reminder;
import com.fsoft.sonic_larue.moviestore.listener.OnGetReminderFinishListener;
import com.fsoft.sonic_larue.moviestore.util.Util;

import java.util.ArrayList;

/**
 * Created by DungHT8 on 2015/12/08.
 */
public class NavigationModel implements INavigationModel {

    private DatabaseHelper databaseHelper;
    private Context context;

    public NavigationModel(Context context) {
        this.context = context;
        this.databaseHelper = new DatabaseHelper(context);
    }

    @Override
    public void fetchReminder(OnGetReminderFinishListener reminderFinishListener) {
        ArrayList<Reminder> reminders = new ArrayList<>();
        ArrayList<Reminder> arrayList = databaseHelper.getReminders();
        if (arrayList.size() > 0) {
            for (int i = 0; i < arrayList.size(); i++) {
                reminders.add(arrayList.get(i));
                if (i == 1) {
                    break;
                }
            }
            reminderFinishListener.onSuccess(reminders);
        } else {
            reminderFinishListener.onSuccess(reminders);
        }

    }

    @Override
    public void fetchProfile(OnGetReminderFinishListener reminderFinishListener, int profileId) {
        Profile profile = databaseHelper.getProfile();
        if (profile != null) {
            reminderFinishListener.onSuccessGetProfile(profile);
        }

    }
}

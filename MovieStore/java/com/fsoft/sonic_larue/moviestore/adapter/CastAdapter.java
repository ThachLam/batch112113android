package com.fsoft.sonic_larue.moviestore.adapter;

/**
 * Created by DungHT8 on 2015/11/23.
 */

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.fsoft.sonic_larue.moviestore.R;
import com.fsoft.sonic_larue.moviestore.application.MyApplication;
import com.fsoft.sonic_larue.moviestore.entity.Cast;
import com.fsoft.sonic_larue.moviestore.ui.fragment.PreferencesFragment;
import com.fsoft.sonic_larue.moviestore.util.GlobalVariable;
import com.fsoft.sonic_larue.moviestore.util.Util;
import com.fsoft.sonic_larue.moviestore.ui.MainActivity;

import java.util.Collections;
import java.util.List;


/**
 * Created by DungHT8 on 2015/11/23.
 */
public class CastAdapter extends RecyclerView.Adapter<CastAdapter.MyViewHolder> {
    List<Cast> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
    private MainActivity activity;

    public CastAdapter(Context context, List<Cast> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
        activity = (MainActivity) context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.fragment_cast_row, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Cast current = data.get(position);
        holder.name.setText(current.getName());

        ImageLoader imageLoader = MyApplication.getInstance().getImageLoader();
        holder.poster.setImageUrl(GlobalVariable.API_IMAGE_W1851 + current.getProfilePath(), imageLoader);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        NetworkImageView poster;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            poster = (NetworkImageView) itemView.findViewById(R.id.poster);

            int actionBarHeight = 0;
            TypedValue tv = new TypedValue();
            if (activity.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, activity.getResources().getDisplayMetrics());
            }
            int tabBarHeight = (int) activity.getResources().getDimension(R.dimen.tab_layout_height);
            poster.getLayoutParams().height = (int) ((Util.getSizeScreen(activity).x - actionBarHeight - tabBarHeight) * 3 / 9.7);
            poster.getLayoutParams().width = (int) ((Util.getSizeScreen(activity).x - actionBarHeight - tabBarHeight) * 3 / 9.7);


        }
    }
}

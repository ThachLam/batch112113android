package com.fsoft.sonic_larue.moviestore.ui.dialog;

import android.support.v7.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.fsoft.sonic_larue.moviestore.R;
import com.fsoft.sonic_larue.moviestore.ui.fragment.ProfileFragment;

/**
 * Created by DungHT8 on 2015/12/02.
 */
public class PhotoDialog extends DialogFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final String takePhoto =  getResources().getString(R.string.take_photo);
        final String library =  getResources().getString(R.string.choose_from_library);
        final String cancel =  getResources().getString(R.string.cancel);

        final CharSequence[] items = {takePhoto, library};
        return new AlertDialog.Builder(getActivity())
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals(takePhoto)) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            getParentFragment().startActivityForResult(intent, ProfileFragment.REQUEST_CAMERA);
                        } else if (items[item].equals(library)) {
                            Intent intent = new Intent(
                                    Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            intent.setType("image/*");
                            getParentFragment().startActivityForResult(
                                    Intent.createChooser(intent, "Select File"),
                                    ProfileFragment.SELECT_FILE);
                        } else if (items[item].equals(cancel)) {
                            dialog.dismiss();
                        }
                    }
                }).create();
    }


}

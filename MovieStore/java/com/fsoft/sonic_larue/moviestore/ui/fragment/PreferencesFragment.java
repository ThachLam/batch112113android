package com.fsoft.sonic_larue.moviestore.ui.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.Toast;

import com.fsoft.sonic_larue.moviestore.R;
import com.fsoft.sonic_larue.moviestore.ui.MainActivity;
import com.fsoft.sonic_larue.moviestore.ui.dialog.RatingDialog;

/**
 * Created by DungHT8 on 2015/11/30.
 */
public class PreferencesFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener, RatingDialog.OnDateSeekBar {
    public static final String TAG = PreferencesFragment.class.getSimpleName();
    private Preference preference;
    SharedPreferences sharedPreferences;
    private OnChangePreference onChangeSetting;
    private MainActivity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.setting);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        init();

        activity = (MainActivity) getActivity();
        setHasOptionsMenu(true);
    }

    private void init() {
        ListPreference category = (ListPreference) findPreference("category");
        ListPreference soft = (ListPreference) findPreference("soft");
        EditTextPreference year = (EditTextPreference) findPreference("year");
        preference = findPreference("rating");

        category.setSummary(category.getEntry());
        soft.setSummary(soft.getEntry());
        year.setSummary(year.getText());

        final float rate = sharedPreferences.getFloat("rate", 1.0f);
        preference.setSummary(String.valueOf(rate));


        preference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                RatingDialog ratingDialog = new RatingDialog();
                Bundle bundle = new Bundle();
                bundle.putFloat("process", sharedPreferences.getFloat("rate", 1.0f));
                ratingDialog.setArguments(bundle);
                ratingDialog.show(getFragmentManager(), "test");
                ratingDialog.setSeekBarListener(PreferencesFragment.this);

                return false;
            }
        });

        onChangeSetting = (OnChangePreference) getParentFragment();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen()
                .getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);

    }

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {

    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen()
                .getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference pref = findPreference(key);

        if (pref instanceof ListPreference) {
            ListPreference listPref = (ListPreference) pref;
            pref.setSummary(listPref.getEntry());
        } else if (pref instanceof EditTextPreference) {
            EditTextPreference editPref = (EditTextPreference) pref;
            //check format year
            try {
                Integer.parseInt(editPref.getText());
                pref.setSummary(editPref.getText());

            } catch (NumberFormatException ex) {
                sharedPreferences.edit().putString("year", "0").commit();
                pref.setSummary("0");
                Toast.makeText(getActivity(), getResources().getString(R.string.error_format_year), Toast.LENGTH_SHORT).show();
                return;

            }

        }

        if (key == "soft") {
            onChangeSetting.onSoftChange();
        } else {
            onChangeSetting.onFilterChange();
        }
    }


    @Override
    public void onReceive(float process) {
        preference.setSummary(String.valueOf(process));
        sharedPreferences.edit().putFloat("rate", process).commit();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (activity.getCurrentTab() == MainActivity.TAB_SETTING_INDEX) {
            activity.getSupportActionBar().setTitle(activity.getResources().getString(R.string.setting));
        }
    }

    public interface OnChangePreference {
        void onSoftChange();

        void onFilterChange();
    }
}

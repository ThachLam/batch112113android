package com.fsoft.sonic_larue.moviestore.ui.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fsoft.sonic_larue.moviestore.R;
import com.fsoft.sonic_larue.moviestore.adapter.ReminderAdapter;
import com.fsoft.sonic_larue.moviestore.alarm.ScheduleManager;
import com.fsoft.sonic_larue.moviestore.entity.Reminder;
import com.fsoft.sonic_larue.moviestore.listener.ClickListener;
import com.fsoft.sonic_larue.moviestore.navigator.Navigator;
import com.fsoft.sonic_larue.moviestore.present.ReminderPresent;
import com.fsoft.sonic_larue.moviestore.ui.MainActivity;
import com.fsoft.sonic_larue.moviestore.util.ActionEvent;
import com.fsoft.sonic_larue.moviestore.util.ActionEventConstant;
import com.fsoft.sonic_larue.moviestore.view.ReminderView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DungHT8 on 2015/11/23.
 */


/**
 * Created by DungHT8 on 2015/11/23.
 */

public class ReminderFragment extends BaseFragment implements BaseFragment.OnReminderReceiver, BaseFragment.OnDetailSender, ReminderView {

    public static String TAG = ReminderFragment.class.getSimpleName();

    private RecyclerView recyclerView;
    private ReminderAdapter adapter;
    private ContentLoadingProgressBar contentLoadingProgressBar;
    private List<Reminder> reminders;
    private TextView emptyData;


    private int currentReminderID;
    private OnReminderSender onReminderSender;
    private OnDetailReceiver onDetailReceiver;
    private ReminderPresent reminderPresent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_reminder, container, false);
        initView(layout);
        return layout;
    }

    private void initView(View layout) {
        recyclerView = (RecyclerView) layout.findViewById(R.id.movies);
        contentLoadingProgressBar = (ContentLoadingProgressBar) layout.findViewById(R.id.loading);
        reminders = new ArrayList<>();
        emptyData = (TextView) layout.findViewById(R.id.empty);
        adapter = new ReminderAdapter(activity, reminders, Reminder.FRAGMENT);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(activity, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Bundle bundle = new Bundle();
                int movieId = reminders.get(position).getMovieId();
                bundle.putInt("movieId", movieId);
                bundle.putInt("position", position);

                ActionEvent actionEvent = new ActionEvent();
                actionEvent.parentFragment = ReminderFragment.this;
                actionEvent.bundle = bundle;
                actionEvent.tag = ActionEventConstant.DETAIL_MOVIE_FRAGMENT;

                Navigator.getInstance().handleSwitchFragment(actionEvent);
            }

            @Override
            public void onLongClick(View view, int position) {
                currentReminderID = position;
            }
        }));

        reminderPresent = new ReminderPresent(this, getContext());
        onReminderSender = (OnReminderSender) getParentFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        reminderPresent.fetchReminder();
    }


    @Override
    public void onReminderReceiveUpdateReminder(int movieId, String date) {
        reminderPresent.fetchReminder();
        if (onDetailReceiver != null) {
            onDetailReceiver.onDetailReceiveUpdateReminder(movieId, date);
        }
    }

    @Override
    public void onReminderReceiveDeleteReminder() {
        if (onDetailReceiver != null) {
            onDetailReceiver.onDetailReceiveDeleteReminder();
        }
    }

    @Override
    public void onReminderReceiveUpdateFavourite(int movieId, boolean isFavourite) {
        if (onDetailReceiver != null) {
            onDetailReceiver.onDetailReceiveFavourite(movieId);
        }
    }

    @Override
    public void onDetailSendFavourite(int movieId, boolean isFavourite) {
        onReminderSender.onReminderUpdateFavourite(movieId, isFavourite);
    }

    @Override
    public void onDetailSendReminder(int movieId, String date) {
        onReminderSender.onReminderUpdateReminder(movieId, date);
    }

    @Override
    public void showProgress() {
        contentLoadingProgressBar.show();
    }

    @Override
    public void hideProgress() {
        contentLoadingProgressBar.hide();
    }

    @Override
    public void refreshAdapter() {
        adapter.notifyDataSetChanged();
        if (reminders.size() > 0) {
            emptyData.setVisibility(View.GONE);
        } else {
            emptyData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setDataReminder(List<Reminder> reminders) {
        this.reminders.clear();
        this.reminders.addAll(reminders);
    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildLayoutPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (activity.getCurrentTab() == MainActivity.TAB_SETTING_INDEX) {
            MenuItem menuItem = menu.findItem(R.id.action_menu);
            if (menuItem != null) {
                menuItem.setVisible(false);
            }

            MenuItem searchItem = menu.findItem(R.id.action_search);
            if (searchItem != null) {
                searchItem.setVisible(false);
            }
            activity.getSupportActionBar().setTitle(activity.getResources().getString(R.string.all_reminder));
        }

    }

    public void setOnDetailReceiver(OnDetailReceiver onDetailReceiver) {
        this.onDetailReceiver = onDetailReceiver;
    }

    @Override
    public void onStart() {
        super.onStart();
        registerForContextMenu(recyclerView);
    }

    @Override
    public void onStop() {
        super.onStop();
        unregisterForContextMenu(recyclerView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = activity.getMenuInflater();
        inflater.inflate(R.menu.dialog_confirm_reminder, menu);
    }


    @Override
    public boolean onContextItemSelected(final MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_delete1:
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);

                builder.setTitle(R.string.comfirm_delete)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {

                                int movieID = reminders.get(currentReminderID).getMovieId();
                                long delete = databaseHelper.deleteReminder(movieID);
                                if (delete != -1) {
                                    reminders.remove(currentReminderID);
                                    refreshAdapter();
                                    onReminderSender.onReminderDeleteReminder();
                                    ScheduleManager.cancelAlarm(activity, movieID);
                                }

                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                builder.show();
                break;
            case R.id.action_cancel1:
                break;
        }
        return true;
    }


}
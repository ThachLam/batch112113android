package com.example.admin.app01;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Admin on 8/17/2016.
 */
public class DetailFragment extends Fragment {

    Contact contact;
    TextView tvNameValue;
    TextView tvPhoneValue;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_fragment, container, false);
        tvNameValue = (TextView) view.findViewById(R.id.tvNameValue);
        tvPhoneValue = (TextView) view.findViewById(R.id.tvPhoneValue);
        Bundle bundle = getArguments();
        if (bundle != null) {
            contact = bundle.getParcelable("contact");
        }

        if (contact != null) {
            updateData();
        }

        return view;
    }

    private void updateData() {
        tvNameValue.setText(contact.getName());
        tvPhoneValue.setText(contact.getPhone());
    }

    public void setData(Contact contact) {
        this.contact = contact;
        updateData();
    }
}

package com.fsoft.sonic_larue.moviestore.adapter;

/**
 * Created by DungHT8 on 2015/11/23.
 */

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.fsoft.sonic_larue.moviestore.application.MyApplication;
import com.fsoft.sonic_larue.moviestore.database.DatabaseHelper;
import com.fsoft.sonic_larue.moviestore.entity.Movie;
import com.fsoft.sonic_larue.moviestore.R;
import com.fsoft.sonic_larue.moviestore.util.GlobalVariable;
import com.fsoft.sonic_larue.moviestore.util.Util;
import com.fsoft.sonic_larue.moviestore.ui.fragment.MovieFragment;
import com.fsoft.sonic_larue.moviestore.ui.MainActivity;

import java.util.Collections;
import java.util.List;


/**
 * Created by DungHT8 on 2015/11/23.
 */
public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder> {
    List<Movie> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
    private int mode;
    private DatabaseHelper databaseHelper;
    private ImageLoader imageLoader;
    private MainActivity mainActivity;
    private Fragment fragment;
    private boolean isFavourite;

    public MovieAdapter(Context context, List<Movie> data, int mode, Fragment fragment) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
        this.mode = mode;
        mainActivity = (MainActivity) context;
        databaseHelper = mainActivity.getDatabase();
        imageLoader = MyApplication.getInstance().getImageLoader();
        this.fragment = fragment;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.fragment_movie_row, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Movie current = data.get(position);
        holder.title.setText(current.getTitle());
        holder.titleGrid.setText(current.getTitle());
        // holder.poster.setText(current.getTitle());
        holder.releaseDate.setText(current.getReleaseDate());
        holder.rating.setText(String.valueOf(current.getRating()) + "/10.0");
        holder.overwrite.setText(current.getOverview());

        holder.poster.setImageUrl(GlobalVariable.API_IMAGE_W300 + current.getPoster(), imageLoader);

        //check favourite
        isFavourite = databaseHelper.isFavourite(current.getId());
        current.setFavourite(isFavourite);
        if (isFavourite) {
            holder.star.setImageResource(R.mipmap.ic_start_selected);
        } else {
            holder.star.setImageResource(R.mipmap.ic_star_border_black);
        }
        if (current.isAdult()) {
            holder.adult.setVisibility(View.VISIBLE);
        } else {
            holder.adult.setVisibility(View.INVISIBLE);
        }
        if (mode == MovieFragment.LISTVIEW_MODE) {
            holder.layoutInfo.setVisibility(View.VISIBLE);
            holder.title.setVisibility(View.VISIBLE);
            holder.titleGrid.setVisibility(View.GONE);
            holder.star.setVisibility(View.VISIBLE);
            holder.poster.getLayoutParams().height = (int) (Util.getSizeScreen(mainActivity).x / 3.5);
            holder.poster.getLayoutParams().width = (int) (Util.getSizeScreen(mainActivity).x / 3.5);
        } else {
            holder.title.setVisibility(View.GONE);
            holder.layoutInfo.setVisibility(View.GONE);
            holder.titleGrid.setVisibility(View.VISIBLE);
            holder.star.setVisibility(View.GONE);
            holder.poster.getLayoutParams().height = (int) (Util.getSizeScreen(mainActivity).x / 2.5);
            holder.poster.getLayoutParams().width = (int) (Util.getSizeScreen(mainActivity).x / 2.5);
        }


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        NetworkImageView poster;
        TextView releaseDate;
        TextView rating;
        TextView overwrite;
        LinearLayout layoutInfo;
        TextView titleGrid;
        TextView adult;
        ImageView star;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            titleGrid = (TextView) itemView.findViewById(R.id.titleGrid);

            releaseDate = (TextView) itemView.findViewById(R.id.release_date);
            rating = (TextView) itemView.findViewById(R.id.rating);
            overwrite = (TextView) itemView.findViewById(R.id.overview);
            layoutInfo = (LinearLayout) itemView.findViewById(R.id.layout_info);

            star = (ImageView) itemView.findViewById(R.id.star1);
            adult = (TextView) itemView.findViewById(R.id.adult);

            poster = (NetworkImageView) itemView.findViewById(R.id.poster);
            star.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (fragment instanceof MovieFragment) {
                        if (!isFavourite) {
                            long insert = databaseHelper.insertMovie(data.get(getLayoutPosition()));
                            if (insert != -1) {
                                star.setImageResource(R.mipmap.ic_start_selected);
                                isFavourite = true;
                            }
                        } else {
                            long delete = databaseHelper.deleteMovie(data.get(getLayoutPosition()).getId());
                            if (delete != -1) {
                                star.setImageResource(R.mipmap.ic_star_border_black);
                                isFavourite = false;
                            } else {
                            }
                        }

                        if (fragment instanceof MovieFragment) {
                            MovieFragment movieFragment = (MovieFragment) fragment;
                            movieFragment.sendNotificationUpDateFavourite(data.get(getLayoutPosition()).getId(), isFavourite);

                        }
                    }
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (fragment instanceof MovieFragment) {
                        MovieFragment movieFragment = (MovieFragment) fragment;
                        movieFragment.showDetail(getLayoutPosition());
                    }
                }
            });
        }
    }


}

package com.example.admin.app01;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 8/17/2016.
 */
public class ListFragment extends Fragment implements ListAdapter.OnItemClick {

//    Context context;
    MainActivity context;
    List<Contact> contactList = new ArrayList<>();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (MainActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);
        ListView listView = (ListView) view.findViewById(R.id.listView);
        for (char c = 'A'; c < 'Z'; c++) {
            contactList.add(new Contact("NV" + c, "01111111111" + c));
        }
        ListAdapter listAdapter = new ListAdapter(context, contactList);
        listAdapter.setCallBack(this);
        listView.setAdapter(listAdapter);

        return view;
    }

    @Override
    public void onItemClick(int position) {
        context.showDetail(contactList.get(position));
    }
}

package com.fsoft.sonic_larue.moviestore.listener;

import android.view.View;

/**
 * Created by DungHT8 on 2015/12/08.
 */
public interface ClickListener {
    void onClick(View view, int position);

    void onLongClick(View view, int position);
}

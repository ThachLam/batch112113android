package com.fsoft.sonic_larue.moviestore.ui.fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.fsoft.sonic_larue.moviestore.R;
import com.fsoft.sonic_larue.moviestore.util.GlobalVariable;
import com.fsoft.sonic_larue.moviestore.ui.MainActivity;

/**
 * Created by DungHT8 on 2015/11/23.
 */
public class AboutFragment extends BaseFragment{
    private MainActivity activity;
    boolean loadingFinished = true;
    boolean redirect = false;
    ContentLoadingProgressBar contentLoadingProgressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        activity = (MainActivity)getActivity();
    }

    public static final String TAG = AboutFragment.class.getSimpleName();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_about, container, false);
        WebView webView = (WebView)layout.findViewById(R.id.about);
        contentLoadingProgressBar = (ContentLoadingProgressBar)layout.findViewById(R.id.loading);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(GlobalVariable.URL_ABOUT);
        contentLoadingProgressBar.hide();
//        webView.setWebViewClient(new WebViewClient() {
//
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String urlNewString) {
//                if (!loadingFinished) {
//                    redirect = true;
//                }
//
//                loadingFinished = false;
//                view.loadUrl(GlobalVariable.URL_ABOUT);
//                return true;
//            }
//
//            @Override
//            public void onPageStarted(WebView view, String url, Bitmap facIcon) {
//                loadingFinished = false;
//                //SHOW LOADING IF IT ISNT ALREADY VISIBLE
//                contentLoadingProgressBar.show();
//            }
//
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                if(!redirect){
//                    loadingFinished = true;
//                }
//
//                if(loadingFinished && !redirect){
//                    //HIDE LOADING IT HAS FINISHED
//                    contentLoadingProgressBar.hide();
//                } else{
//                    redirect = false;
//                }
//
//            }
//        });

        return layout;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem menuItem = menu.findItem(R.id.action_menu);
        if (menuItem != null) {
            menuItem.setVisible(false);
        }

        MenuItem searchItem = menu.findItem(R.id.action_search);
        if (searchItem != null) {
            searchItem.setVisible(false);
        }
        activity.getSupportActionBar().setTitle(activity.getResources().getString(R.string.about));
    }
}

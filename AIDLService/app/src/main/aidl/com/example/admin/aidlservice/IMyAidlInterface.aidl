// IMyAidlInterface.aidl
package com.example.admin.aidlservice;

// Declare any non-default types here with import statements

interface IMyAidlInterface {

    int getPid();

    void registerCallBack(IRemoteServiceCallBack callBack);

    void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat,
            double aDouble, String aString);
}

interface IRemoteServiceCallBack {

    void valueChange(int value);
}

package com.fsoft.sonic_larue.moviestore.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fsoft.sonic_larue.moviestore.R;
import com.fsoft.sonic_larue.moviestore.navigator.Navigator;
import com.fsoft.sonic_larue.moviestore.util.ActionEvent;
import com.fsoft.sonic_larue.moviestore.util.ActionEventConstant;

/**
 * Created by DungHT8 on 2015/11/23.
 */
public class SettingFragment extends BaseFragment implements PreferencesFragment.OnChangePreference, BaseFragment.OnReminderSender {
    public static final String TAG = SettingFragment.class.getSimpleName();
    private OnReminderReceiver onReminderReceiver;
    OnChangeSetting onChangeSetting;
    OnSettingSender onSettingSender;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "---------------------------------");
        // setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_setting, container, false);
        gotoPreferencesScreen();
        return layout;
    }

    public void gotoReminder() {
        ActionEvent actionEvent = new ActionEvent();
        actionEvent.parentFragment = this;
        actionEvent.tag = ActionEventConstant.REMINDER_FRAGMENT;

        Navigator.getInstance().handleSwitchFragment(actionEvent);
    }

    public void setOnReminderReceiver(OnReminderReceiver onReminderReceiver) {
        this.onReminderReceiver = onReminderReceiver;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    public void gotoPreferencesScreen() {
        ActionEvent actionEvent = new ActionEvent();
        actionEvent.parentFragment = this;
        actionEvent.tag = ActionEventConstant.PREFERENCE_FRAGMENT;
        Navigator.getInstance().handleSwitchFragment(actionEvent);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onChangeSetting = (OnChangeSetting) context;
        onSettingSender = (OnSettingSender) context;
    }

    public void updateDataReminder(int movieId, String data) {
        if (onReminderReceiver != null) {
            onReminderReceiver.onReminderReceiveUpdateReminder(movieId, data);
        }
    }

    public void updateDataFavourite(int movieId, boolean isFavourite) {
        if (onReminderReceiver != null) {
            onReminderReceiver.onReminderReceiveUpdateFavourite(movieId, isFavourite);
        }
    }

    public void deleteReminder() {
        if (onReminderReceiver != null) {
            onReminderReceiver.onReminderReceiveDeleteReminder();
        }
    }

    @Override
    public void onSoftChange() {
        onChangeSetting.onSoftChange();
    }

    @Override
    public void onFilterChange() {
        onChangeSetting.onFilterChange();

    }

    @Override
    public void onReminderDeleteReminder() {
        onSettingSender.onSettingDeleteReminder();
    }

    @Override
    public void onReminderUpdateFavourite(int movieId, boolean isFavourite) {
        onSettingSender.onSettingUpdateFavourite(movieId, isFavourite);
    }

    @Override
    public void onReminderUpdateReminder(int movieId, String date) {
        onSettingSender.onSettingUpdateReminder(movieId, date);
    }

    public interface OnChangeSetting {
        void onSoftChange();

        void onFilterChange();
    }


}

package com.fsoft.sonic_larue.moviestore.adapter;

/**
 * Created by DungHT8 on 2015/11/23.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.fsoft.sonic_larue.moviestore.R;
import com.fsoft.sonic_larue.moviestore.application.MyApplication;
import com.fsoft.sonic_larue.moviestore.database.DatabaseHelper;
import com.fsoft.sonic_larue.moviestore.entity.Movie;
import com.fsoft.sonic_larue.moviestore.util.GlobalVariable;
import com.fsoft.sonic_larue.moviestore.ui.fragment.MovieFragment;
import com.fsoft.sonic_larue.moviestore.ui.MainActivity;

import java.util.Collections;
import java.util.List;


/**
 * Created by DungHT8 on 2015/11/23.
 */
public class FavouriteAdapter extends RecyclerView.Adapter<FavouriteAdapter.MyViewHolder> {
    List<Movie> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
    private int mode;
    private DatabaseHelper databaseHelper;


    public FavouriteAdapter(Context context, List<Movie> data, int mode) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
        this.mode = mode;
        MainActivity mainActivity = (MainActivity)context;
        databaseHelper = mainActivity.getDatabase();
    }

    public void setMode(int mode){
        this.mode = mode;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.fragment_movie_row, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Movie current = data.get(position);
        holder.title.setText(current.getTitle());
        holder.titleGrid.setText(current.getTitle());
        holder.releaseDate.setText(current.getReleaseDate());
        holder.rating.setText(String.valueOf(current.getRating()));
        holder.overwrite.setText(current.getOverview());

        ImageLoader imageLoader = MyApplication.getInstance().getImageLoader();
        holder.poster.setImageUrl(GlobalVariable.API_IMAGE_W300 + current.getPoster(), imageLoader);

        //check favourite
        if(databaseHelper.isFavourite(current.getId())) {
            holder.star.setImageResource(R.mipmap.ic_start_selected);
        }
        else {
            holder.star.setImageResource(R.mipmap.ic_star_border_black);
        }

        if(mode == MovieFragment.LISTVIEW_MODE){
            holder.layoutInfo.setVisibility(View.VISIBLE);
            holder. title.setVisibility(View.VISIBLE);
            holder.titleGrid.setVisibility(View.GONE);
        }
        else {
            holder.title.setVisibility(View.GONE);
            holder.layoutInfo.setVisibility(View.GONE);
            holder.titleGrid.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        NetworkImageView poster;
        TextView releaseDate;
        TextView rating;
        TextView overwrite;
        LinearLayout layoutInfo;
        TextView titleGrid;
        ImageView star;


        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            titleGrid = (TextView) itemView.findViewById(R.id.titleGrid);
            poster = (NetworkImageView) itemView.findViewById(R.id.poster);
            releaseDate = (TextView) itemView.findViewById(R.id.release_date);
            rating = (TextView) itemView.findViewById(R.id.rating);
            overwrite = (TextView) itemView.findViewById(R.id.overview);
            layoutInfo = (LinearLayout)itemView.findViewById(R.id.layout_info);
            star = (ImageView)itemView.findViewById(R.id.star);

        }
    }
}

package com.example.admin.demoservice;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Admin on 8/19/2016.
 */
public class IntentServiceDemo extends IntentService {

    static final String TAG = IntentServiceDemo.class.getSimpleName();

    public IntentServiceDemo() {
        super("default name");
    }

    int k = 10;
    @Override
    protected void onHandleIntent(Intent intent) {
        String name = Thread.currentThread().getName();
        Log.d(TAG, "onHandleIntent: " + k++ + " on Thread: " + name + " object: " + Thread.currentThread().hashCode());
        for (int i = 0; i < 10; i++) {
            Log.d(TAG, "k = " + k++);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }
}

package com.fsoft.sonic_larue.moviestore.entity;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.preference.PreferenceManager;

/**
 * Created by DungHT8 on 2015/12/01.
 */
public class Setting {
    private float rate;
    private int year;
    private int soft;
    private int category;
    SharedPreferences sp;

    public Setting(Context context) {
        sp = PreferenceManager.getDefaultSharedPreferences(context);
        category = Integer.parseInt(sp.getString("category", "1"));
        year =  Integer.parseInt(sp.getString("year", "0"));
        rate = sp.getFloat("rate", 1.0f);
        soft = Integer.parseInt(sp.getString("soft", "1"));
    }
    public void getData() {
        if(sp != null) {
            category = Integer.parseInt(sp.getString("category", "1"));
            year = Integer.parseInt(sp.getString("year", "0"));
            rate = sp.getFloat("rate", 1.0f);
            soft = Integer.parseInt(sp.getString("soft", "1"));
        }
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getSoft() {
        return soft;
    }

    public void setSoft(int soft) {
        this.soft = soft;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }
}

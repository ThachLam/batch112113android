package com.example.admin.demoservice;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class DemoMessengerServiceActivity extends AppCompatActivity {

    static final String TAG = DemoMessengerServiceActivity.class.getSimpleName();
    Messenger mMessenger;
    boolean mBound = false;
    TextView tvN;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    tvN.setText(String.valueOf(msg.arg1));
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    };
    Messenger messenger = new Messenger(handler);

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mMessenger = new Messenger(service);
            Message msg = Message.obtain(null, 1000);
            msg.replyTo = messenger;
            try {
                mMessenger.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mMessenger = null;
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo_messenger_service);

        tvN = (TextView) findViewById(R.id.tvN);

        Intent i = new Intent("com.test.messengerService");
        bindService(i, mConnection, BIND_AUTO_CREATE);
    }

    public void send(View view) throws RemoteException {
        EditText txtMess = (EditText) findViewById(R.id.txtMess);
        Message msg = Message.obtain(null, MessengerService.MSG_SAY_HELLO);
        Bundle b = new Bundle();
        b.putString("msg", txtMess.getText().toString());
        msg.setData(b);
        mMessenger.send(msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }
}

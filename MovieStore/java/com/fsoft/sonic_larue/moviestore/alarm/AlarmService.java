package com.fsoft.sonic_larue.moviestore.alarm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.fsoft.sonic_larue.moviestore.R;
import com.fsoft.sonic_larue.moviestore.database.DatabaseHelper;
import com.fsoft.sonic_larue.moviestore.entity.Reminder;
import com.fsoft.sonic_larue.moviestore.ui.MainActivity;

/**
 * Created by DungHT8 on 2015/11/29.
 */
public class AlarmService extends Service {
    private NotificationManager notificationManager;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                int movieId = bundle.getInt("movieId");
                //show notification
                Reminder reminder = databaseHelper.getReminder(movieId);
                if (reminder != null) {
                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                            .setSmallIcon(R.mipmap.ic_launcher) // notification icon
                            .setTicker(reminder.getTitle())
                            .setContentTitle(reminder.getTitle()) // title for notification
                            .setContentText("Year: " + reminder.getYear() + " Rate: " + reminder.getRate() + "/10") // message for notification
                            .setAutoCancel(true); // clear notification after click
                    Intent intent1 = new Intent(this, MainActivity.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    PendingIntent pi = PendingIntent.getActivity(this, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.setContentIntent(pi);
                    mBuilder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
                    NotificationManager mNotificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    mNotificationManager.notify(0, mBuilder.build());

                }
                //Delete alarm
                long delete = databaseHelper.deleteReminder(movieId);

                if (delete != -1) {
                    Intent intentAlarm = new Intent("Alarm");
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intentAlarm);
                }
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}

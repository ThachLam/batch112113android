package com.fsoft.sonic_larue.moviestore.ui.fragment;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;


import com.fsoft.sonic_larue.moviestore.R;

/**
 * Created by DungHT8 on 2015/11/23.
 */
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.TextView;
;
import com.fsoft.sonic_larue.moviestore.adapter.MovieAdapter;
import com.fsoft.sonic_larue.moviestore.entity.Movie;
import com.fsoft.sonic_larue.moviestore.listener.ClickListener;
import com.fsoft.sonic_larue.moviestore.navigator.Navigator;
import com.fsoft.sonic_larue.moviestore.present.FavouritePresent;
import com.fsoft.sonic_larue.moviestore.present.IFavouritePresent;
import com.fsoft.sonic_larue.moviestore.util.ActionEvent;
import com.fsoft.sonic_larue.moviestore.util.ActionEventConstant;
import com.fsoft.sonic_larue.moviestore.view.FavouriteView;


import java.util.ArrayList;
import java.util.List;


/**
 * Created by DungHT8 on 2015/11/23.
 */
public class FavouriteFragment extends BaseFragment implements BaseFragment.OnDetailSender, SearchView.OnQueryTextListener, FavouriteView {

    private static String TAG = MovieFragment.class.getSimpleName();

    private RecyclerView recyclerView;
    private MovieAdapter adapter;
    private ContentLoadingProgressBar contentLoadingProgressBar;
    private List<Movie> movies;
    private int currentPosition;
    private TextView emptyData;


    private OnFavouriteSender onFavouriteSender;
    private OnDetailReceiver onDetailReceiver;

    private String filter = "";

    private IFavouritePresent favouritePresent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        favouritePresent = new FavouritePresent(this, getContext());

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_favourite, container, false);
        init(layout);
        return layout;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        activity.getMenuInflater().inflate(R.menu.menu_favourite, menu);
        super.onCreateOptionsMenu(menu, inflater);
        Log.d(TAG, "-------------ON CREATE OPTION MENU FAVOURITE-------------");

        activity.getSupportActionBar().setTitle(activity.getResources().getString(R.string.favourite));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

            SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                @Override
                public boolean onQueryTextSubmit(String s) {
                    Log.d(TAG, "onQueryTextSubmit ");
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    Log.d(TAG, "onQueryTextChange " + s);
                    filter = s.toLowerCase();
                    favouritePresent.searchData(filter);
                    return false;
                }
            });
        }
    }

    private void init(View layout) {
        recyclerView = (RecyclerView) layout.findViewById(R.id.movies);
        contentLoadingProgressBar = (ContentLoadingProgressBar) layout.findViewById(R.id.loading);
        movies = new ArrayList<>();
        emptyData = (TextView) layout.findViewById(R.id.empty);
        adapter = new MovieAdapter(activity, movies, MovieFragment.LISTVIEW_MODE, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(activity, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putInt("movieId", movies.get(position).getId());
                bundle.putInt("position", position);
                bundle.putParcelable("object", movies.get(position));

                ActionEvent actionEvent = new ActionEvent();
                actionEvent.parentFragment = FavouriteFragment.this;
                actionEvent.bundle = bundle;
                actionEvent.tag = ActionEventConstant.DETAIL_MOVIE_FRAGMENT;

                Navigator.getInstance().handleSwitchFragment(actionEvent);
            }

            @Override
            public void onLongClick(View view, int position) {
                currentPosition = position;
            }
        }));


    }

    @Override
    public void onResume() {
        super.onResume();
        favouritePresent.fetchData(filter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onFavouriteSender = (OnFavouriteSender) context;
    }

    @Override
    public void onStart() {
        super.onStart();
        registerForContextMenu(recyclerView);
    }

    @Override
    public void onStop() {
        super.onStop();
        unregisterForContextMenu(recyclerView);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = activity.getMenuInflater();
        inflater.inflate(R.menu.dialog_confirm, menu);
    }

    @Override
    public boolean onContextItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);

                builder.setTitle(R.string.comfirm_delete)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {

                                int movieID = movies.get(currentPosition).getId();
                                long delete = databaseHelper.deleteMovie(movieID);
                                if (delete != -1) {
                                    movies.remove(currentPosition);
                                    refreshAdapter();
                                    onFavouriteSender.onFavouriteUpdateFavourite(movieID, false);
                                }

                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                builder.show();

                break;
            case R.id.action_cancel:
                break;
        }
        return false;
    }

    @Override
    public void onDetailSendFavourite(int movieId, boolean isFavourite) {
        favouritePresent.fetchData(filter);
        onFavouriteSender.onFavouriteUpdateFavourite(movieId, isFavourite);
    }

    @Override
    public void onDetailSendReminder(int movieId, String date) {
        onFavouriteSender.onFavouriteUpdateReminder(movieId, date);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void showProgress() {
        contentLoadingProgressBar.show();

    }

    @Override
    public void hideProgress() {
        contentLoadingProgressBar.hide();
    }

    @Override
    public void refreshAdapter() {
        adapter.notifyDataSetChanged();
        if (movies.size() > 0) {
            emptyData.setVisibility(View.GONE);
        } else {
            emptyData.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void setDataFavourite(List<Movie> movies) {
        this.movies.clear();
        this.movies.addAll(movies);
        refreshAdapter();

        if (onDetailReceiver != null) {
            onDetailReceiver.onDetailReceiveFavourite(1);
        }

    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }


    public void updateDataFavourite(int movieId, boolean isFavourite) {
        favouritePresent.fetchData(filter);
    }

    public void updateDataReminder(int movieId, String date) {
        if (onDetailReceiver != null) {
            onDetailReceiver.onDetailReceiveUpdateReminder(movieId, date);
        }
    }

    public void deleteReminder() {
        if (onDetailReceiver != null) {
            onDetailReceiver.onDetailReceiveDeleteReminder();
        }
    }
    public void setOnDetailReceiver(OnDetailReceiver onDetailReceiver){
        this.onDetailReceiver = onDetailReceiver;
    }
}
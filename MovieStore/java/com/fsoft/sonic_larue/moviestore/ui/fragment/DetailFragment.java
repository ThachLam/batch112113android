package com.fsoft.sonic_larue.moviestore.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fsoft.sonic_larue.moviestore.R;
import com.fsoft.sonic_larue.moviestore.application.MyApplication;
import com.fsoft.sonic_larue.moviestore.entity.Cast;
import com.fsoft.sonic_larue.moviestore.util.GlobalVariable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by DungHT8 on 2015/12/08.
 */
public class DetailFragment extends BaseFragment{
    public static final String TAG =  DetailFragment.class.getSimpleName();
    FragmentPagerAdapter adapterViewPager;
    int pesonId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle  bundle = getArguments();
        if (bundle!= null) {
           // pesonId = pesonId.
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.from(getActivity()).inflate(R.layout.fragment_detail, container, false);
        ViewPager vpPager = (ViewPager) view.findViewById(R.id.viewpager);
        adapterViewPager = new MyPagerAdapter(getChildFragmentManager());
        vpPager.setAdapter(adapterViewPager);
        return view;
    }

    public void fetchData() {
        String url = GlobalVariable.getApiCastCrewDetail(pesonId);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonCast) {
                        Log.d(TAG, jsonCast.toString());
                        try {
                            JSONArray jsonArray = jsonCast.getJSONArray("cast");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
//                                Cast cast = new Cast();
//                                cast.setCastId(object.getInt(Cast.CAST_ID));
//                                cast.setName(object.getString(Cast.NAME));
//                                cast.setProfilePath(object.getString(Cast.PROFILE_PATH));
//                                cast.setCreditId(object.getString(Cast.CREDIT_ID));
//                                casts.add(cast);
                            }
                           // adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
             //   loadingProgressBar.hide();
            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }

    public static class MyPagerAdapter extends FragmentPagerAdapter {
        private static int NUM_ITEMS = 3;

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: // Fragment # 0 - This will show FirstFragment
                    return DetailCastFragment.newInstance(0, "Page # 1");
                case 1: // Fragment # 0 - This will show FirstFragment different title
                    return DetailCastFragment.newInstance(1, "Page # 2");
                case 2: // Fragment # 1 - This will show SecondFragment
                    return DetailCastFragment.newInstance(2, "Page # 3");
                default:
                    return null;
            }
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return "Page " + position;
        }

    }
}

package com.fsoft.sonic_larue.moviestore.ui.fragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.fsoft.sonic_larue.moviestore.R;
import com.fsoft.sonic_larue.moviestore.adapter.CastAdapter;
import com.fsoft.sonic_larue.moviestore.application.MyApplication;
import com.fsoft.sonic_larue.moviestore.entity.Cast;
import com.fsoft.sonic_larue.moviestore.entity.Movie;
import com.fsoft.sonic_larue.moviestore.entity.Reminder;
import com.fsoft.sonic_larue.moviestore.listener.ClickListener;
import com.fsoft.sonic_larue.moviestore.present.DetailViewPresent;
import com.fsoft.sonic_larue.moviestore.present.IDetailViewPresent;
import com.fsoft.sonic_larue.moviestore.util.GlobalVariable;
import com.fsoft.sonic_larue.moviestore.util.Util;
import com.fsoft.sonic_larue.moviestore.util.Validate;
import com.fsoft.sonic_larue.moviestore.ui.MainActivity;
import com.fsoft.sonic_larue.moviestore.view.DetailMovieView;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by DungHT8 on 2015/11/25.
 */
public class DetailMovieFragment extends BaseFragment implements View.OnClickListener, BaseFragment.OnDetailReceiver, DetailMovieView {
    public static final String TAG = DetailMovieFragment.class.getSimpleName();

    private TextView releaseDate;
    private TextView rating;
    private TextView overwrite;
    private TextView infoReminder;

    private NetworkImageView imageView;
    private ArrayList<Cast> casts;

    private RecyclerView recyclerView;
    private CastAdapter adapter;

    private TextView adult;
    private ImageView star;

    private Movie currentMovie;
    private boolean isFavourite;

    private Button reminder;
    private Calendar calendar;

    private BaseFragment.OnDetailSender onDetailSender;

    private ContentLoadingProgressBar loadingProgressBar;
    private IDetailViewPresent detailViewPresent;
    private ImageLoader imageLoader;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentMovie = new Movie();

        detailViewPresent = new DetailViewPresent(this, getContext());

        setHasOptionsMenu(true);
        imageLoader = MyApplication.getInstance().getImageLoader();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_detail_movie, container, false);

        initView(layout);
        layout.setClickable(true);
        return layout;
    }

    @Override
    public void onResume() {
        super.onResume();
        detailViewPresent.fetchMovie(getArguments());

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_empty, menu);

        MenuItem menuItem = menu.findItem(R.id.action_menu);

        if (currentMovie != null && getParentFragment() instanceof ReminderFragment && activity.getCurrentTab() == MainActivity.TAB_SETTING_INDEX) {
            initOptionMenu(menuItem, menu);

        } else if (getParentFragment() instanceof MovieFragment && activity.getCurrentTab() == MainActivity.TAB_MOVIE_INDEX) {
            initOptionMenu(menuItem, menu);

        } else if (getParentFragment() instanceof FavouriteFragment && activity.getCurrentTab() == MainActivity.TAB_FAVOURITE_INDEX) {
            initOptionMenu(menuItem, menu);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    public void initOptionMenu(MenuItem menuItem, Menu menu) {
        activity.getSupportActionBar().setTitle(currentMovie.getTitle());
        if (menuItem != null) {
            menuItem.setVisible(false);
        }

        MenuItem searchItem = menu.findItem(R.id.action_search);
        if (searchItem != null) {
            searchItem.setVisible(false);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    private void initView(View layout) {
        releaseDate = (TextView) layout.findViewById(R.id.release_date);
        rating = (TextView) layout.findViewById(R.id.rating);
        overwrite = (TextView) layout.findViewById(R.id.overview);
        loadingProgressBar = (ContentLoadingProgressBar) layout.findViewById(R.id.loading);
        imageView = (NetworkImageView) layout.findViewById(R.id.poster);
        imageView.getLayoutParams().height = Util.getSizeScreen(getActivity()).x * 2 / 5;
        imageView.getLayoutParams().width = Util.getSizeScreen(getActivity()).x * 2 / 5;

        star = (ImageView) layout.findViewById(R.id.star);
        star.setOnClickListener(this);

        adult = (TextView) layout.findViewById(R.id.adult);

        casts = new ArrayList<>();

        recyclerView = (RecyclerView) layout.findViewById(R.id.cast);
        adapter = new CastAdapter(getActivity(), casts);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(layoutManager);
        reminder = (Button) layout.findViewById(R.id.reminder);
        reminder.setOnClickListener(this);

        infoReminder = (TextView) layout.findViewById(R.id.info_reminder);
        onAttachFragment(getParentFragment());
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(activity, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                activity.gotoDetailCast();
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));

    }

    public void passData(int movieId, boolean isFavourite) {
        onDetailSender.onDetailSendFavourite(movieId, isFavourite);
    }

    public void onAttachFragment(Fragment fragment) {
        try {
            onDetailSender = (BaseFragment.OnDetailSender) fragment;

        } catch (ClassCastException e) {
            throw new ClassCastException(
                    fragment.toString() + " must implement OnPlayerSelectionSetListener");
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.star:
                checkFavourite();
                onDetailSender.onDetailSendFavourite(currentMovie.getId(), isFavourite);
                break;
            case R.id.reminder:
                calendar = Calendar.getInstance();
                new DatePickerDialog(getActivity(), d, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
        }
    }

    DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            if (view.isShown()) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                new TimePickerDialog(getActivity(), t, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true).show();
            }

        }
    };
    TimePickerDialog.OnTimeSetListener t = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay,
                              int minute) {
            if (view.isShown()) {
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);
                if (Validate.isValidReminder(calendar)) {
                    detailViewPresent.updateReminder(currentMovie, calendar);
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.error_message_date), Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    public void checkFavourite() {
        if (!isFavourite) {
            long insert = databaseHelper.insertMovie(currentMovie);
            if (insert != -1) {
                star.setImageResource(R.mipmap.ic_start_selected);
                isFavourite = true;
            } else {
                Toast.makeText(activity, getResources().getString(R.string.error_db), Toast.LENGTH_LONG).show();
            }
        } else {
            long delete = databaseHelper.deleteMovie(currentMovie.getId());
            if (delete != -1) {
                star.setImageResource(R.mipmap.ic_star_border_black);
                isFavourite = false;
            } else {
                Toast.makeText(activity, getResources().getString(R.string.error_db), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onDetailReceiveFavourite(int position) {
        isFavourite = databaseHelper.isFavourite(currentMovie.getId());
        if (isFavourite) {
            star.setImageResource(R.mipmap.ic_start_selected);
        } else {
            star.setImageResource(R.mipmap.ic_star_border_black);
        }
    }

    @Override
    public void onDetailReceiveUpdateReminder(int movieID, String date) {
        if (movieID == currentMovie.getId()) {
            infoReminder.setText(date);
        }
    }

    @Override
    public void onDetailReceiveDeleteReminder() {
        infoReminder.setText("");
    }

    @Override
    public void showProgress() {
        loadingProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        loadingProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void refreshAdapter() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void setDataCast(List<Cast> casts) {
        this.casts.addAll(casts);
    }

    @Override
    public void setDataMovie(Movie movie) {
        this.currentMovie = movie;
        overwrite.setText(currentMovie.getOverview());
        rating.setText(String.valueOf(currentMovie.getRating()) + "/10.0");
        releaseDate.setText(currentMovie.getReleaseDate());
        imageView.setImageUrl(GlobalVariable.API_IMAGE_W300 + currentMovie.getPoster(), imageLoader);

        if (currentMovie.isAdult()) {
            adult.setVisibility(View.VISIBLE);
        } else {
            adult.setVisibility(View.INVISIBLE);
        }
        isFavourite = databaseHelper.isFavourite(currentMovie.getId());
        if (isFavourite) {
            star.setImageResource(R.mipmap.ic_start_selected);
        } else {
            star.setImageResource(R.mipmap.ic_star_border_black);
        }

        Reminder reminder = databaseHelper.getReminder(currentMovie.getId());
        if (reminder != null) {
            infoReminder.setText(reminder.getDate());
        }
        // activity.getSupportActionBar().setTitle(currentMovie.getTitle());
        activity.getSupportActionBar().setTitle(currentMovie.getTitle());
        detailViewPresent.fetchCast(currentMovie.getId());
    }

    @Override
    public void updateReminder(String date) {
        infoReminder.setText(date);
        onDetailSender.onDetailSendReminder(currentMovie.getId(), date);
    }

    @Override
    public void updateFavourite(boolean isFavourite) {
        if (isFavourite) {
            star.setImageResource(R.mipmap.ic_start_selected);
        } else {
            star.setImageResource(R.mipmap.ic_star_border_black);
        }
    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildLayoutPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildLayoutPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }


    }
}

package com.fsoft.sonic_larue.moviestore.navigator;

import com.fsoft.sonic_larue.moviestore.util.ActionEvent;

/**
 * Created by DungHT8 on 2015/12/09.
 */
public interface INavigator {
    void handleSwitchFragment(ActionEvent actionEvent);
}

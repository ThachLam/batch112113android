package com.fsoft.sonic_larue.moviestore.model;

import com.fsoft.sonic_larue.moviestore.listener.OnGetReminderFinishListener;

/**
 * Created by DungHT8 on 2015/12/08.
 */
public interface INavigationModel {
    void fetchReminder(OnGetReminderFinishListener reminderFinishListener) ;
    void fetchProfile(OnGetReminderFinishListener reminderFinishListener, int profileId) ;
}

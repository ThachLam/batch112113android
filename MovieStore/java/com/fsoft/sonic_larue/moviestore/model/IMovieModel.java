package com.fsoft.sonic_larue.moviestore.model;

import com.fsoft.sonic_larue.moviestore.listener.OnGetMoviesFinishListener;
import com.fsoft.sonic_larue.moviestore.entity.Setting;

/**
 * Created by DungHT8 on 2015/12/05.
 */
public interface IMovieModel {
    void fetchMovie(OnGetMoviesFinishListener onGetDataFinishListener, int numberPage, Setting setting);
}

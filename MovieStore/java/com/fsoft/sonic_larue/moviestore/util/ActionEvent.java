package com.fsoft.sonic_larue.moviestore.util;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

/**
 * Created by DungHT8 on 2015/11/24.
 */
public class ActionEvent {
    public Bundle bundle;
    public int tag;
    public Fragment parentFragment;
    public FragmentActivity fragmentActivity;


}

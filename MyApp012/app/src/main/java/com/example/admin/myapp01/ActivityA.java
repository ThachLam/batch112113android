package com.example.admin.myapp01;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Admin on 8/1/2016.
 */
public class ActivityA extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_a);
    }
}

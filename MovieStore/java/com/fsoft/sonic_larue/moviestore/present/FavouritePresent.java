package com.fsoft.sonic_larue.moviestore.present;

import android.content.Context;
import android.util.Log;

import com.fsoft.sonic_larue.moviestore.entity.Movie;
import com.fsoft.sonic_larue.moviestore.listener.OnGetMoviesFinishListener;
import com.fsoft.sonic_larue.moviestore.model.FavouriteModel;
import com.fsoft.sonic_larue.moviestore.model.IFavouriteModel;
import com.fsoft.sonic_larue.moviestore.view.FavouriteView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DungHT8 on 2015/12/08.
 */
public class FavouritePresent implements IFavouritePresent, OnGetMoviesFinishListener {
    FavouriteView favouriteView;
    Context context;
    IFavouriteModel favouriteModel;

    public FavouritePresent(FavouriteView favouriteView, Context context) {
        this.favouriteView = favouriteView;
        this.context = context;
        this.favouriteModel = new FavouriteModel(context);
    }

    @Override
    public void fetchData(String filter) {
        favouriteView.showProgress();
        favouriteModel.fetchData(filter, this);
    }

    @Override
    public void searchData(String filter) {
        favouriteView.showProgress();
        favouriteModel.searchData(filter,this);
    }

    @Override
    public void onSuccess(List<Movie> movies) {
        favouriteView.hideProgress();
        favouriteView.setDataFavourite(movies);

    }

    @Override
    public void onError() {
        favouriteView.hideProgress();
    }
}

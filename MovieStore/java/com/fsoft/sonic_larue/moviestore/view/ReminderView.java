package com.fsoft.sonic_larue.moviestore.view;

import com.fsoft.sonic_larue.moviestore.entity.Reminder;

import java.util.List;

/**
 * Created by DungHT8 on 2015/12/07.
 */
public interface ReminderView {
    void showProgress();

    void hideProgress();

    void refreshAdapter();

    void setDataReminder(List<Reminder> reminders);

}

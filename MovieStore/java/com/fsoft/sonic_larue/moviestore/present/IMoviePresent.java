package com.fsoft.sonic_larue.moviestore.present;

import com.fsoft.sonic_larue.moviestore.entity.Setting;

/**
 * Created by DungHT8 on 2015/12/05.
 */
public interface IMoviePresent {
     void fetchMovie(int numberPage, Setting setting);

}

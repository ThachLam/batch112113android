package com.fsoft.sonic_larue.moviestore.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.fsoft.sonic_larue.moviestore.adapter.MovieAdapter;
import com.fsoft.sonic_larue.moviestore.entity.Movie;
import com.fsoft.sonic_larue.moviestore.R;
import com.fsoft.sonic_larue.moviestore.entity.Setting;
import com.fsoft.sonic_larue.moviestore.navigator.Navigator;
import com.fsoft.sonic_larue.moviestore.present.IMoviePresent;
import com.fsoft.sonic_larue.moviestore.present.MoviePresent;
import com.fsoft.sonic_larue.moviestore.util.ActionEvent;
import com.fsoft.sonic_larue.moviestore.util.ActionEventConstant;
import com.fsoft.sonic_larue.moviestore.util.DateTime;
import com.fsoft.sonic_larue.moviestore.util.GlobalVariable;
import com.fsoft.sonic_larue.moviestore.util.Util;
import com.fsoft.sonic_larue.moviestore.view.MovieView;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;


/**
 * Created by DungHT8 on 2015/11/23.
 */
public class MovieFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, BaseFragment.OnDetailSender, MovieView {

    private static String TAG = MovieFragment.class.getSimpleName();
    public static final int LISTVIEW_MODE = 100;
    public static final int GRIDVIEW_MODE = 201;

    private RecyclerView recyclerView;
    private MovieAdapter adapter;
    private int mode = LISTVIEW_MODE;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<Movie> movies;
    private TextView emptyData;


    private boolean loading = true;
    private int currentPage = 1;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    //setting
    private Setting setting;
    private ContentLoadingProgressBar loadingProgressBar;

    private OnMovieSender onMovieSender;
    private OnDetailReceiver onDetailReceiver;
    private IMoviePresent moviePresent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Util.setDefaultAuthen();
        Bundle bundle = getArguments();
        if (bundle != null) {
            mode = bundle.getInt("mode");
        }
        setHasOptionsMenu(true);
        setting = new Setting(getActivity());
        getDataSetting();
        moviePresent = new MoviePresent(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onMovieSender = (OnMovieSender) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_movie, container, false);
        recyclerView = (RecyclerView) layout.findViewById(R.id.movies);
        swipeRefreshLayout = (SwipeRefreshLayout) layout.findViewById(R.id.swipe_refresh_layout);
        loadingProgressBar = (ContentLoadingProgressBar) layout.findViewById(R.id.loading);
        emptyData = (TextView) layout.findViewById(R.id.empty);

        movies = new ArrayList<>();
        adapter = new MovieAdapter(getActivity(), movies, mode, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(getLayoutManager(mode));


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    //Call your method here for next set of data
                    if (mode == LISTVIEW_MODE) {
                        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                        visibleItemCount = layoutManager.getChildCount();
                        totalItemCount = layoutManager.getItemCount();
                        pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    } else {
                        GridLayoutManager layoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
                        visibleItemCount = layoutManager.getChildCount();
                        totalItemCount = layoutManager.getItemCount();
                        pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    }

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            //loading = false;
                            //Action here
                            currentPage++;
                            moviePresent.fetchMovie(currentPage, setting);
                        }
                    }
                }
            }
        });
        swipeRefreshLayout.setOnRefreshListener(this);
        // fetchData();
        //    moviePresent.fetchMovie(currentPage, setting);
        return layout;
    }

    @Override
    public void onResume() {
        super.onResume();
        moviePresent.fetchMovie(currentPage, setting);
    }

    public void getDataSetting() {
        setting.getData();
    }

    public void changeLayout(int mode) {
        MovieAdapter adapter = (MovieAdapter) recyclerView.getAdapter();
        //change data
        adapter.setMode(mode);
        this.mode = mode;

        //change layout
        recyclerView.setLayoutManager(getLayoutManager(mode));

        synchronized (adapter) {
            refreshAdapter();
        }
    }

    public RecyclerView.LayoutManager getLayoutManager(int mode) {
        RecyclerView.LayoutManager layoutManager = null;
        switch (mode) {
            case GRIDVIEW_MODE:
                layoutManager = new GridLayoutManager(getActivity(), 2);
                break;
            case LISTVIEW_MODE:
                layoutManager = new LinearLayoutManager(getActivity());
                break;
            default:
                break;
        }
        return layoutManager;
    }


    @Override
    public void onRefresh() {
        movies.clear();
        currentPage = 1;
        swipeRefreshLayout.setRefreshing(true);
        moviePresent.fetchMovie(currentPage, setting);
    }


    public void sendNotificationUpDateFavourite(int movieId, boolean isFavourite) {
        onMovieSender.onMovieUpdateFavourite(movieId, isFavourite);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        getActivity().getMenuInflater().inflate(R.menu.menu_movie, menu);
        MenuItem item = menu.findItem(R.id.action_menu);
        if (item != null) {
            if (mode == MovieFragment.LISTVIEW_MODE) {
                item.setIcon(R.mipmap.ic_grid_on_white);
            } else {
                item.setIcon(R.mipmap.ic_list_white);
            }
        }

        if (setting != null) {
            String title;
            switch (setting.getCategory()) {
                case GlobalVariable.POPULAR_MOVIE:
                    title = getResources().getString(R.string.popular_movies);
                    break;
                case GlobalVariable.NOW_PLAYING_MOVIE:
                    title = getResources().getString(R.string.now_playing_movies);
                    break;
                case GlobalVariable.TOP_RATE_MOVIE:
                    title = getResources().getString(R.string.top_rated_movies);
                    break;
                case GlobalVariable.UPCOMING_MOVIE:
                    title = getResources().getString(R.string.upcoming_movies);
                    break;
                default:
                    title = getResources().getString(R.string.popular_movies);
                    break;

            }
            activity.getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_menu:
                if (mode == MovieFragment.LISTVIEW_MODE) {
                    mode = MovieFragment.GRIDVIEW_MODE;
                    item.setIcon(R.mipmap.ic_list_white);
                } else {
                    mode = MovieFragment.LISTVIEW_MODE;
                    item.setIcon(R.mipmap.ic_grid_on_white);
                }
                changeLayout(mode);
                return true;

        }
        return true;
    }

    @Override
    public void onDetailSendFavourite(int movieId, boolean isFavourite) {
        //update data
        refreshAdapter();

        //send response to main activity
        onMovieSender.onMovieUpdateFavourite(movieId, isFavourite);
    }

    @Override
    public void onDetailSendReminder(int movieId, String date) {
        onMovieSender.onMovieUpdateReminder(movieId, date);
    }


    public static MovieFragment newInstance(int mode) {
        MovieFragment fragment = new MovieFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("mode", mode);
        fragment.setArguments(bundle);
        return fragment;
    }

    public void updateDataFavourite(int movieId, boolean isFavourite) {
        refreshAdapter();
        if (onDetailReceiver != null) {
            onDetailReceiver.onDetailReceiveFavourite(1);
        }
    }

    public void updateDataReminder(int movieId, String date) {
        refreshAdapter();
        if (onDetailReceiver != null) {
            onDetailReceiver.onDetailReceiveUpdateReminder(movieId, date);
        }
    }

    public void deleteReminder() {
        if (onDetailReceiver != null) {
            onDetailReceiver.onDetailReceiveDeleteReminder();
        }
    }

    public void updateFilter() {
        currentPage = 1;
        movies.clear();
        setting.getData();
        moviePresent.fetchMovie(currentPage, setting);

    }

    public void updateSoft() {
        setting.getData();
        Collections.sort(movies, new MyMovieComp());
        refreshAdapter();
    }

    public void showDetail(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt("movieId", movies.get(position).getId());
        bundle.putInt("position", position);
        bundle.putParcelable("object", movies.get(position));

        ActionEvent actionEvent = new ActionEvent();
        actionEvent.parentFragment = this;
        actionEvent.bundle = bundle;
        actionEvent.tag = ActionEventConstant.DETAIL_MOVIE_FRAGMENT;

        Navigator.getInstance().handleSwitchFragment(actionEvent);
    }

    @Override
    public void showProgress() {
        loadingProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        loadingProgressBar.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void refreshAdapter() {
        adapter.notifyDataSetChanged();
        if (movies.size() > 0) {
            emptyData.setVisibility(View.GONE);
        } else {
            emptyData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setDataMovies(List<Movie> movies) {
        Collections.sort(movies, new MyMovieComp());
        this.movies.addAll(movies);
        refreshAdapter();
    }

    public void setOnDetailReceiver(OnDetailReceiver onDetailReceiver){
        this.onDetailReceiver = onDetailReceiver;
    }
    class MyMovieComp implements Comparator<Movie> {
        @Override
        public int compare(Movie lhs, Movie rhs) {
            if (setting.getSoft() == 2) {
                if (lhs.getRating() > rhs.getRating()) {
                    return -1;
                }
                return 1;
            } else {
                Date dateParent = DateTime.toDate(lhs.getReleaseDate());
                Date date = DateTime.toDate(rhs.getReleaseDate());
                if (dateParent.getTime() < date.getTime()) {
                    return 1;
                }
                return -1;
            }
        }
    }

}

package com.example.admin.demoservice;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;

public class MessengerService extends Service {

    static final String TAG = MessengerService.class.getSimpleName();
    static final int MSG_SAY_HELLO = 1;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "binding...");

        new Thread(new Runnable() {
            int k;
            @Override
            public void run() {
                while(true) {
                    // update to Ativity
                    if (activityMessenger != null) {
                        Message msg = Message.obtain(null, 1, k++, 0);
                        try {
                            activityMessenger.send(msg);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        return mMessenger.getBinder();
    }

    Messenger activityMessenger;

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1000:
                    activityMessenger = msg.replyTo;
                    break;
                case MSG_SAY_HELLO:
                    Log.d(TAG, msg.getData().getString("msg"));
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    final Messenger mMessenger = new Messenger(new IncomingHandler());

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }
}

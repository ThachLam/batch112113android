package com.fsoft.sonic_larue.moviestore.ui.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.fsoft.sonic_larue.moviestore.R;
import com.fsoft.sonic_larue.moviestore.database.DatabaseHelper;
import com.fsoft.sonic_larue.moviestore.entity.Profile;
import com.fsoft.sonic_larue.moviestore.util.DateTime;
import com.fsoft.sonic_larue.moviestore.util.Util;
import com.fsoft.sonic_larue.moviestore.util.Validate;
import com.fsoft.sonic_larue.moviestore.ui.CircleImageView;
import com.fsoft.sonic_larue.moviestore.ui.dialog.PhotoDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

/**
 * Created by DungHT8 on 2015/11/23.
 */
public class ProfileFragment extends BaseFragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private Button cancel;
    private Button done;

    private EditText inputName, inputEmail;
    private EditText date;
    private RadioButton male, female;
    private TextInputLayout inputLayoutName, inputLayoutEmail, inputLayoutDate;
    public static final String TAG = ProfileFragment.class.getSimpleName();
    private CircleImageView imageView;
    private Calendar calendar;
    private DatabaseHelper databaseHelper;
    private Profile profile;
    private OnUpdateProfile updateProfile;
    public static final int REQUEST_CAMERA = 100;
    public static final int SELECT_FILE = 101;
    public Bitmap bitmap = null;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_profile, container, false);
        cancel = (Button) layout.findViewById(R.id.cancel);
        cancel.setOnClickListener(this);
        done = (Button) layout.findViewById(R.id.done);
        done.setOnClickListener(this);
        imageView = (CircleImageView) layout.findViewById(R.id.image_profile);
        imageView.setOnClickListener(this);
        imageView.getLayoutParams().width = Util.getSizeScreen(activity).x / 3;
        imageView.getLayoutParams().height = Util.getSizeScreen(activity).x / 3;

        calendar = Calendar.getInstance();

        date = (EditText) layout.findViewById(R.id.date);
        date.setOnClickListener(this);

        male = (RadioButton) layout.findViewById(R.id.male);
        female = (RadioButton) layout.findViewById(R.id.female);

        male.setOnCheckedChangeListener(this);
        female.setOnCheckedChangeListener(this);

        inputLayoutName = (TextInputLayout) layout.findViewById(R.id.input_layout_name);
        inputLayoutEmail = (TextInputLayout) layout.findViewById(R.id.input_layout_email);
        inputName = (EditText) layout.findViewById(R.id.input_name);
        inputEmail = (EditText) layout.findViewById(R.id.input_email);
        inputLayoutDate = (TextInputLayout) layout.findViewById(R.id.input_date);

        inputName.addTextChangedListener(new MyTextWatcher(inputName));
        inputEmail.addTextChangedListener(new MyTextWatcher(inputEmail));
        date.addTextChangedListener(new MyTextWatcher(date));
        databaseHelper = new DatabaseHelper(getActivity());
        showProfile();
        return layout;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel:
                getActivity().onBackPressed();
                break;
            case R.id.done:
                if (validateName() && validateEmail() && validateBirth()) {
                    if (profile == null) {
                        profile = new Profile();
                    }
                    profile.setEmail(inputEmail.getText().toString());
                    profile.setBirth(date.getText().toString());
                    profile.setName(inputName.getText().toString());
                    if (bitmap != null) {
                        profile.setBitmap(Util.getBytes(bitmap));
                    }
                    if (male.isChecked()) {
                        profile.setGender(1);
                    } else {
                        profile.setGender(2);
                    }
                    if (databaseHelper.getProfile() != null) {
                        long update = databaseHelper.updateProfile(profile);
                        if (update != -1) {
                            Log.d(TAG, "Update profile success ---" + profile.getId());
                        }
                    } else {
                        long insertProfile = databaseHelper.insertProfile(profile);
                        if (insertProfile != -1) {
                            Log.d(TAG, "insert profile success ----" + profile.getId());
                        }
                    }
                    updateProfile.onUpdateProfile(1);
                    getActivity().onBackPressed();
                }
                break;
            case R.id.date:
                new DatePickerDialog(getActivity(), d, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.HOUR_OF_DAY)).show();
                break;
            case R.id.image_profile:
                new PhotoDialog().show(getChildFragmentManager(), TAG);
                break;
        }
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showProfile() {
        Profile profile = databaseHelper.getProfile();
        if (profile != null) {
            inputEmail.setText(profile.getEmail());

            calendar.setTime(DateTime.toDate(profile.getBirth()));
            date.setText(profile.getBirth());
            int genderText = profile.getGender();
            if (genderText == 1) {
                male.setChecked(true);
            } else {
                female.setChecked(true);
            }
            inputName.setText(profile.getName());
            if (profile.getBitmap() != null) {
                bitmap = Util.getImage(profile.getBitmap());
                imageView.setImageBitmap(bitmap);
            }

        } else {

        }
    }

    private boolean validateName() {
        if (inputName.getText().toString().trim().isEmpty()) {
            inputLayoutName.setError(getString(R.string.error_name));
            requestFocus(inputName);
            return false;
        } else {
            inputLayoutName.setErrorEnabled(false);
        }

        return true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        updateProfile = (OnUpdateProfile) context;
    }

    private boolean validateEmail() {
        String email = inputEmail.getText().toString().trim();

        if (email.isEmpty() || !Validate.isValidEmail(email)) {
            inputLayoutEmail.setError(getString(R.string.error_email));
            requestFocus(inputEmail);
            return false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateBirth() {
        if (!Validate.isValidBirth(date.getText().toString())) {
            inputLayoutDate.setError(getString(R.string.error_birth_day));
            return false;
        } else {
            inputLayoutDate.setErrorEnabled(false);
        }
        return true;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_name:
                    validateName();
                    break;
                case R.id.input_email:
                    validateEmail();
                    break;
                case R.id.date:
                    validateBirth();
                    break;

            }
        }
    }

    DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            if (view.isShown()) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                if (!Validate.isValidBirth(DateTime.toString(calendar.getTime()))) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.error_message_date), Toast.LENGTH_SHORT).show();
                } else {
                    date.setText(DateTime.toString(calendar.getTime()));
                }
            }
        }
    };

    public interface OnUpdateProfile {
        void onUpdateProfile(int profileId);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
     //   Toast.makeText(getActivity(), "Photo", Toast.LENGTH_SHORT).show();
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                imageView.setImageBitmap(bitmap);

            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(getActivity(), selectedImageUri, projection, null, null,
                        null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();

                String selectedImagePath = cursor.getString(column_index);

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bitmap = BitmapFactory.decodeFile(selectedImagePath, options);

                imageView.setImageBitmap(bitmap);
            }
        }
    }

    public void hideKeyboard() {
        Util.hideKeyboard(getActivity(), inputEmail);
        Util.hideKeyboard(getActivity(), inputName);
    }

}

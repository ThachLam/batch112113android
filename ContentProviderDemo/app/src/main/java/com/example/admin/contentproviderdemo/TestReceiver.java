package com.example.admin.contentproviderdemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

/**
 * Created by Admin on 8/31/2016.
 */
public class TestReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        String value = bundle.getString("key");
        Toast.makeText(context, "Data: " + value, Toast.LENGTH_LONG).show();
    }
}

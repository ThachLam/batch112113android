package com.fsoft.sonic_larue.moviestore.model;

import android.content.Context;
import android.util.Log;

import com.fsoft.sonic_larue.moviestore.database.DatabaseHelper;
import com.fsoft.sonic_larue.moviestore.entity.Movie;
import com.fsoft.sonic_larue.moviestore.listener.OnGetMoviesFinishListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DungHT8 on 2015/12/08.
 */
public class FavouriteModel implements IFavouriteModel {
    private Context context;
    private DatabaseHelper databaseHelper;

    public FavouriteModel(Context context) {
        this.context = context;
        this.databaseHelper = new DatabaseHelper(context);
    }

    @Override
    public void fetchData(String filter, OnGetMoviesFinishListener onGetMoviesFinishListener) {

        ArrayList<Movie> movies;
        if (!filter.equals("")) {
            movies = databaseHelper.getMovies(filter);
        } else {
            movies = databaseHelper.getMovies();

        }
        if (movies != null) {
            onGetMoviesFinishListener.onSuccess(movies);
        } else {
            onGetMoviesFinishListener.onError();
        }

    }

    @Override
    public void searchData(String filter, OnGetMoviesFinishListener onGetMoviesFinishListener) {
        List<Movie> movies = databaseHelper.getMovies();
        if (movies != null) {
            for (int i = 0; i < movies.size(); i++) {
                Movie movie = movies.get(i);
                if (!movie.getTitle().toLowerCase().contains(filter)) {
                    movies.remove(i);
                    i--;
                }
            }
            onGetMoviesFinishListener.onSuccess(movies);
        } else {
            onGetMoviesFinishListener.onError();
        }
    }
}

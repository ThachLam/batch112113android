package com.fsoft.sonic_larue.moviestore.view;

import com.fsoft.sonic_larue.moviestore.entity.Cast;
import com.fsoft.sonic_larue.moviestore.entity.Movie;
import com.fsoft.sonic_larue.moviestore.entity.Reminder;

import java.util.List;

/**
 * Created by DungHT8 on 2015/12/07.
 */
public interface DetailMovieView {
    void showProgress();

    void hideProgress();

    void refreshAdapter();

    void setDataCast(List<Cast> casts);

    void setDataMovie(Movie movie);

    void updateReminder(String date);

    void updateFavourite(boolean isFavourite);
}

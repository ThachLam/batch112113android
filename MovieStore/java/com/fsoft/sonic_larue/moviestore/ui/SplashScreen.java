package com.fsoft.sonic_larue.moviestore.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.fsoft.sonic_larue.moviestore.R;
import com.fsoft.sonic_larue.moviestore.util.GlobalVariable;


/**
 * Created by DungHT8 on 2015/11/23.
 */
public class SplashScreen extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, GlobalVariable.TIME_SPLASH);
//        String url = "http://api.themoviedb.org/3/configuration?api_key=e7631ffcb8e766993e5ec0c1f4245f93";
//        Intent i = new Intent(Intent.ACTION_VIEW);
//        i.setData(Uri.parse(url));
//        startActivity(i);
    }


}

package com.fsoft.sonic_larue.moviestore.ui.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.fsoft.sonic_larue.moviestore.R;

/**
 * Created by DungHT8 on 2015/12/02.
 */
public class NetworkDialog extends DialogFragment{
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String context = getResources().getString(R.string.error_network);
        String gotoSetting = getResources().getString(R.string.go_setting);
        String cancel = getResources().getString(R.string.cancel);
        return  new AlertDialog.Builder(getActivity())
                // Set Dialog Message
                .setMessage(context)

                        // Positive button
                .setPositiveButton(cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Do something else
                    }
                })
                .setNegativeButton(gotoSetting, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent=new Intent(Settings.ACTION_WIFI_SETTINGS);
                        startActivity(intent);
                        dismiss();
                    }
                })
                .create();
    }
}

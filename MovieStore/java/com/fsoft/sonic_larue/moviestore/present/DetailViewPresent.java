package com.fsoft.sonic_larue.moviestore.present;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.fsoft.sonic_larue.moviestore.entity.Cast;
import com.fsoft.sonic_larue.moviestore.entity.Movie;
import com.fsoft.sonic_larue.moviestore.listener.OnGetCastFinishListener;
import com.fsoft.sonic_larue.moviestore.listener.OnGetMovieFinishListener;
import com.fsoft.sonic_larue.moviestore.listener.OnGetDetailFinishListener;
import com.fsoft.sonic_larue.moviestore.model.DetailViewModel;
import com.fsoft.sonic_larue.moviestore.model.IDetailMovieModel;
import com.fsoft.sonic_larue.moviestore.view.DetailMovieView;

import java.util.Calendar;
import java.util.List;

/**
 * Created by DungHT8 on 2015/12/08.
 */
public class DetailViewPresent implements IDetailViewPresent, OnGetDetailFinishListener {
    private DetailMovieView detailMovieView;
    private Context context;
    private IDetailMovieModel detailViewModel;

    public DetailViewPresent(DetailMovieView detailMovieView, Context context) {
        this.detailMovieView = detailMovieView;
        this.context = context;
        this.detailViewModel = new DetailViewModel(context);
    }

    @Override
    public void fetchCast(int movieID) {
        detailMovieView.showProgress();
        detailViewModel.fetchCast(this, movieID);
    }

    @Override
    public void fetchMovie(Bundle bundle) {
        detailMovieView.showProgress();
        detailViewModel.fetchMovie(this, bundle);

    }

    @Override
    public void updateReminder(Movie movie, Calendar calendar) {
        detailViewModel.updateReminder(movie, calendar, this);
    }



    @Override
    public void onSuccessMovie(Movie movie) {
        detailMovieView.hideProgress();
        detailMovieView.setDataMovie(movie);

    }

    @Override
    public void onErrorMovie() {
        detailMovieView.hideProgress();
    }



    @Override
    public void onSuccessUpdate(String date) {
        detailMovieView.updateReminder(date);

    }

    @Override
    public void onErrorUpdate() {
        detailMovieView.hideProgress();
    }

    @Override
    public void onSuccessCasts(List<Cast> casts) {
        detailMovieView.hideProgress();
        detailMovieView.setDataCast(casts);
        detailMovieView.refreshAdapter();
    }

    @Override
    public void onErrorCast() {
        detailMovieView.hideProgress();
    }
}

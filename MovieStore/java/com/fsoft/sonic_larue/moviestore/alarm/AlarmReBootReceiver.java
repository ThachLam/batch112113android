package com.fsoft.sonic_larue.moviestore.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.fsoft.sonic_larue.moviestore.database.DatabaseHelper;
import com.fsoft.sonic_larue.moviestore.entity.Reminder;
import com.fsoft.sonic_larue.moviestore.util.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by DungHT8 on 2015/11/30.
 */
public class AlarmReBootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(Util.DATE_FORMAT);
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        List<Reminder> reminders = databaseHelper.getReminders();
        if (reminders != null) {
            for (int i = 0; i < reminders.size(); i++) {
                Reminder reminder = reminders.get(i);
                try {
                    Date date;
                    date = dateFormat.parse(reminder.getDate());
                    ScheduleManager.setAlarm(context, date.getTime(), reminder.getMovieId());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

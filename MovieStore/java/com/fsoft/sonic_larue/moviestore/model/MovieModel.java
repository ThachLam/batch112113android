package com.fsoft.sonic_larue.moviestore.model;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fsoft.sonic_larue.moviestore.application.MyApplication;
import com.fsoft.sonic_larue.moviestore.listener.OnGetMoviesFinishListener;
import com.fsoft.sonic_larue.moviestore.entity.Movie;
import com.fsoft.sonic_larue.moviestore.entity.Setting;
import com.fsoft.sonic_larue.moviestore.util.GlobalVariable;
import com.fsoft.sonic_larue.moviestore.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DungHT8 on 2015/12/05.
 */
public class MovieModel implements IMovieModel {
    public MovieModel() {

    }

    @Override
    public void fetchMovie(final OnGetMoviesFinishListener onGetDataFinishListener, final int numberPage, final Setting setting) {
        String url = GlobalVariable.getApiMovies(numberPage, setting.getCategory());
        Util.setDefaultAuthen();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            List<Movie> movies = new ArrayList<>();
                            JSONArray jsonArray = response.getJSONArray("results");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonMovie = jsonArray.getJSONObject(i);

                                //Filter
                                String releaseDate = jsonMovie.getString(Movie.RELEASE_DATE);
                                String strYear = releaseDate.split("-")[0];
                                int year = Integer.parseInt(strYear);
                                float rating = Float.parseFloat(jsonMovie.getString(Movie.VOTE_AVERAGE));
                                boolean checkRate = rating - setting.getRate() > 0.0 || rating == setting.getRate();
                                boolean checkYear = year - setting.getYear() > 0 || year == setting.getYear();
                                if (!(checkRate && checkYear)) {
                                    continue;
                                }
                                Movie movie = new Movie();
                                movie.setId(jsonMovie.getInt(Movie.ID));
                                movie.setAdult(jsonMovie.getBoolean(Movie.ADULT));
                                movie.setPoster(jsonMovie.getString(Movie.POSTER));
                                movie.setOverview(jsonMovie.getString(Movie.OVERVIEW));
                                movie.setReleaseDate(jsonMovie.getString(Movie.RELEASE_DATE));
                                movie.setRating((float) jsonMovie.getDouble(Movie.VOTE_AVERAGE));
                                movie.setTitle(jsonMovie.getString(Movie.TITLE));
                                movies.add(movie);
                            }
                            // Collections.sort(movies, new MyMovieComp());
                            onGetDataFinishListener.onSuccess(movies);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            onGetDataFinishListener.onError();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                // loadingProgressBar.hide();
                // swipeRefreshLayout.setRefreshing(false);
                onGetDataFinishListener.onError();

            }
        });
        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }
}

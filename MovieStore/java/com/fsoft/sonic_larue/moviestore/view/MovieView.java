package com.fsoft.sonic_larue.moviestore.view;

import com.fsoft.sonic_larue.moviestore.entity.Movie;

import java.util.List;

/**
 * Created by DungHT8 on 2015/12/05.
 */
public interface MovieView {
    void showProgress();

    void hideProgress();

    void refreshAdapter();

    void setDataMovies(List<Movie> movies);
}

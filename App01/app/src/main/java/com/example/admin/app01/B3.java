package com.example.admin.app01;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class B3 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b1);
        Log.d("B2", "onCreate");

        Button btnOpenB1 = (Button) findViewById(R.id.btnOpenB1);
        btnOpenB1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(B3.this, B1.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("B2", "onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("B2", "onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("B2", "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("B2", "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("B2", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("B2", "onDestroy");
    }
}

package com.fsoft.sonic_larue.moviestore.adapter;

/**
 * Created by DungHT8 on 2015/11/23.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.fsoft.sonic_larue.moviestore.R;
import com.fsoft.sonic_larue.moviestore.application.MyApplication;
import com.fsoft.sonic_larue.moviestore.entity.Reminder;
import com.fsoft.sonic_larue.moviestore.util.GlobalVariable;
import com.fsoft.sonic_larue.moviestore.util.Util;
import com.fsoft.sonic_larue.moviestore.ui.MainActivity;

import java.util.Collections;
import java.util.List;


/**
 * Created by DungHT8 on 2015/11/23.
 */
public class ReminderAdapter extends RecyclerView.Adapter<ReminderAdapter.MyViewHolder> {
    List<Reminder> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
    private MainActivity activity;
    private ImageLoader imageLoader;
    private int mode;

    public ReminderAdapter(Context context, List<Reminder> data, int mode) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        activity = (MainActivity) context;
        this.data = data;
        imageLoader = MyApplication.getInstance().getImageLoader();
        this.mode = mode;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.fragment_reminder_row, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Reminder current = data.get(position);
        holder.title.setText(current.getTitle() +" - "+current.getYear()+" - "+ current.getRate() +"/10.0");
        holder.date.setText(current.getDate());
        holder.poster.setImageUrl(GlobalVariable.API_IMAGE_W1851 + current.getPosterPath(), imageLoader);

        //ImageLoader imageLoader = MyApplication.getInstance().getImageLoader();
        ///holder.poster.setImageUrl(GlobalVariable.API_IMAGE + current.getProfilePath(), imageLoader);


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        NetworkImageView poster;
        TextView date;
        LinearLayout layout_image;
        ImageView arrow;

        public MyViewHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.date);
            title = (TextView) itemView.findViewById(R.id.title);
            poster = (NetworkImageView) itemView.findViewById(R.id.poster);
            layout_image = (LinearLayout)itemView.findViewById(R.id.layout_image);
            arrow = (ImageView)itemView.findViewById(R.id.arrow);
            if (mode == Reminder.DRAWER) {
                poster.getLayoutParams().height = Util.getSizeScreen(activity).x / 6;
                poster.getLayoutParams().width = 0;
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) layout_image.getLayoutParams();
                params.weight = 0;
                layout_image.setLayoutParams(params);
                arrow.setVisibility(View.GONE);
            } else {
                poster.getLayoutParams().height = Util.getSizeScreen(activity).x / 6;
                poster.getLayoutParams().width = Util.getSizeScreen(activity).x / 6;
                arrow.setVisibility(View.VISIBLE);
            }


        }
    }
}

package com.example.admin.app01;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Admin on 8/10/2016.
 */
public class ListAdapter extends BaseAdapter {

    Context context;
    List<Contact> contactList;
    OnItemClick callBack;

    public void setCallBack(OnItemClick callBack) {
        this.callBack = callBack;
    }

    public ListAdapter(Context context, List<Contact> contactList) {
        this.contactList = contactList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return contactList.size();
    }

    @Override
    public Object getItem(int position) {
        return contactList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item, parent, false);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBack.onItemClick(position);
                }
            });

            viewHolder = new ViewHolder();
            viewHolder.image = (ImageView) convertView.findViewById(R.id.photo);
            viewHolder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            viewHolder.tvPhone = (TextView) convertView.findViewById(R.id.tvPhone);
            viewHolder.btnNext = (Button) convertView.findViewById(R.id.btnNext);
            convertView.setTag(viewHolder);
        }
        viewHolder = (ViewHolder) convertView.getTag();

        Contact c = contactList.get(position);
        viewHolder.tvName.setText(c.name);
        viewHolder.tvPhone.setText(c.phone);

        return convertView;
    }

    interface OnItemClick {
        void onItemClick(int position);
    }

    class ViewHolder {
        ImageView image;
        TextView tvName;
        TextView tvPhone;
        Button btnNext;
    }
}

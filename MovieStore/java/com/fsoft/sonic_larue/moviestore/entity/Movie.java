package com.fsoft.sonic_larue.moviestore.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DungHT8 on 2015/11/23.
 */
public class Movie implements Parcelable {
    private int _id;
    private int id;
    private String title;
    private String poster;
    private float rating;
    private boolean favourite;
    private String overview;
    private String releaseDate;
    private boolean adult;

    public static final String ID_TABLE = "_id";
    public static final String ADULT = "adult";
    public static final String ID = "id";
    public static final String OVERVIEW = "overview";
    public static final String RELEASE_DATE = "release_date";
    public static final String POSTER = "poster_path";
    public static final String TITLE = "title";
    public static final String VOTE_AVERAGE = "vote_average";
    public static final String VOTE_COUNT = "vote_count";
    public static final String BACKDROP_PATH = "backdrop_path";

    protected Movie(Parcel in) {
        _id = in.readInt();
        id = in.readInt();
        title = in.readString();
        poster = in.readString();
        rating = in.readFloat();
        favourite = in.readByte() != 0;
        overview = in.readString();
        releaseDate = in.readString();
        adult = in.readByte() != 0;
    }

    public Movie(){}

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public boolean isAdult() {
        return adult;
    }


    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(_id);
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(poster);
        dest.writeFloat(rating);
        dest.writeByte((byte) (favourite ? 1 : 0));
        dest.writeString(overview);
        dest.writeString(releaseDate);
        dest.writeByte((byte) (adult ? 1 : 0));

    }
}

package com.fsoft.sonic_larue.moviestore.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fsoft.sonic_larue.moviestore.R;

/**
 * Created by DungHT8 on 2015/12/08.
 */
public class DetailCastFragment extends BaseFragment {
    String title;
    int page;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");
    }

    public static DetailCastFragment newInstance(int page, String title) {
        DetailCastFragment detailCastFragment = new DetailCastFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        detailCastFragment.setArguments(args);
        return detailCastFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.from(getActivity()).inflate(R.layout.fragment_detail_cast, container, false);
        TextView tvLabel = (TextView) view.findViewById(R.id.label);
        tvLabel.setText(page + " -- " + title);
        return view;
    }
}

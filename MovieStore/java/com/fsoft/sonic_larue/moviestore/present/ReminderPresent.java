package com.fsoft.sonic_larue.moviestore.present;

import android.content.Context;

import com.fsoft.sonic_larue.moviestore.entity.Profile;
import com.fsoft.sonic_larue.moviestore.entity.Reminder;
import com.fsoft.sonic_larue.moviestore.listener.OnGetReminderFinishListener;
import com.fsoft.sonic_larue.moviestore.model.IReminderModel;
import com.fsoft.sonic_larue.moviestore.model.ReminderModel;
import com.fsoft.sonic_larue.moviestore.view.ReminderView;

import java.util.List;

/**
 * Created by DungHT8 on 2015/12/08.
 */
public class ReminderPresent implements IReminderPresent, OnGetReminderFinishListener {
    private ReminderView reminderView;
    private Context context;
    private IReminderModel reminderModel;

    public ReminderPresent(ReminderView reminderView, Context context) {
        this.reminderView = reminderView;
        this.context = context;
        this.reminderModel = new ReminderModel(context);
    }

    @Override
    public void fetchReminder() {
        this.reminderView.showProgress();
        this.reminderModel.fetchReminder(this);
    }

    @Override
    public void onSuccess(List<Reminder> reminders) {
        this.reminderView.hideProgress();
        this.reminderView.setDataReminder(reminders);
        this.reminderView.refreshAdapter();
    }

    @Override
    public void onError() {
        this.reminderView.hideProgress();
    }

    @Override
    public void onSuccessGetProfile(Profile profile) {

    }
}

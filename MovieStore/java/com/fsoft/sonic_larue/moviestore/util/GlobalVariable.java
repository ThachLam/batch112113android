package com.fsoft.sonic_larue.moviestore.util;

/**
 * Created by DungHT8 on 2015/11/23.
 */
public class GlobalVariable {
    public static long TIME_SPLASH = 3000;

    public static final String SERVER = "http://api.themoviedb.org/3/";
    public static final String API_BASE = SERVER + "configuration?api_key=e7631ffcb8e766993e5ec0c1f4245f93";
    public static final String API_MOVIE_LIST_POPULAR = SERVER + "movie/popular?api_key=e7631ffcb8e766993e5ec0c1f4245f93&page=";
    public static final String API_MOVIE_LIST_TOP_RATED = SERVER + "movie/top_rated?api_key=e7631ffcb8e766993e5ec0c1f4245f93&page=";
    public static final String API_MOVIE_LIST_UP_COMING = SERVER + "movie/upcoming?api_key=e7631ffcb8e766993e5ec0c1f4245f93&page=";
    public static final String API_MOVIE_LIST_NOW_PLAYING = SERVER + "movie/now_playing?api_key=e7631ffcb8e766993e5ec0c1f4245f93&page=";

    private static final String API_MOVIE_DETAILS = "?api_key=e7631ffcb8e766993e5ec0c1f4245f93";
    private static final String API_CAST_CREW_LIST = "/credits?api_key=e7631ffcb8e766993e5ec0c1f4245f93";
    private static final String API_CAST_CREW_DETAIL = "?api_key=e7631ffcb8e766993e5ec0c1f4245f93";

    public static final String API_SEARCH_MOVIES = SERVER + "search/movie?api_key=e7631ffcb8e766993e5ec0c1f4245f93&query=";
    public static final String API_IMAGE_W300 = "http://image.tmdb.org/t/p/w300";
    public static final String API_IMAGE_W1851 = "http://image.tmdb.org/t/p/w185";
    public static final String API_IMAGE_500 = "http://image.tmdb.org/t/p/w500";

    public static final int POPULAR_MOVIE = 100;
    public static final int TOP_RATE_MOVIE = 200;
    public static final int UPCOMING_MOVIE = 300;
    public static final int NOW_PLAYING_MOVIE = 400;

    public static final String getApiMovieDetails(int movieId) {
        return SERVER + "movie/" + movieId + API_MOVIE_DETAILS;
    }

    public static final String getApiMovies(int currentPage, int category) {
        String api;
        switch (category) {
            case POPULAR_MOVIE:
                api = API_MOVIE_LIST_POPULAR;
                break;
            case TOP_RATE_MOVIE:
                api = API_MOVIE_LIST_TOP_RATED;
                break;
            case UPCOMING_MOVIE:
                api = API_MOVIE_LIST_UP_COMING;
                break;
            case NOW_PLAYING_MOVIE:
                api = API_MOVIE_LIST_NOW_PLAYING;
                break;
            default:
                api = API_MOVIE_LIST_POPULAR;
                break;
        }
        return api + currentPage;
    }


    public static final String getApiCastCrewList(int movieId) {
        return SERVER + "movie/" + movieId + API_CAST_CREW_LIST;
    }
    public static final String getApiCastCrewDetail(int personId) {
        return SERVER +  + personId + API_CAST_CREW_LIST;
    }

    public static final String URL_ABOUT = "https://www.themoviedb.org/about/our-history";

}

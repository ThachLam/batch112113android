package com.fsoft.sonic_larue.moviestore.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fsoft.sonic_larue.moviestore.R;
import com.fsoft.sonic_larue.moviestore.database.DatabaseHelper;
import com.fsoft.sonic_larue.moviestore.entity.Movie;
import com.fsoft.sonic_larue.moviestore.navigator.Navigator;
import com.fsoft.sonic_larue.moviestore.ui.fragment.AboutFragment;
import com.fsoft.sonic_larue.moviestore.ui.fragment.BaseFragment;
import com.fsoft.sonic_larue.moviestore.ui.fragment.DetailFragment;
import com.fsoft.sonic_larue.moviestore.ui.fragment.FavouriteFragment;
import com.fsoft.sonic_larue.moviestore.ui.fragment.MovieFragment;
import com.fsoft.sonic_larue.moviestore.ui.fragment.NavigationFragment;
import com.fsoft.sonic_larue.moviestore.ui.fragment.PreferencesFragment;
import com.fsoft.sonic_larue.moviestore.ui.fragment.ProfileFragment;
import com.fsoft.sonic_larue.moviestore.ui.fragment.SettingFragment;
import com.fsoft.sonic_larue.moviestore.util.ActionEvent;
import com.fsoft.sonic_larue.moviestore.util.ActionEventConstant;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends BaseActivity implements NavigationFragment.FragmentDrawerListener, BaseFragment.OnSettingSender, BaseFragment.OnMovieSender, FavouriteFragment.OnFavouriteSender, SettingFragment.OnChangeSetting, ProfileFragment.OnUpdateProfile {
    public static final int TAB_MOVIE_INDEX = 0;
    public static final int TAB_FAVOURITE_INDEX = 1;
    public static final int TAB_SETTING_INDEX = 2;
    public static final int TAB_ABOUT_INDEX = 3;

    TabLayout tabLayout;
    ViewPager viewPager;
    private NavigationFragment fragmentDrawer;
    private DatabaseHelper databaseHelper;


    private int[] tabIcons = {
            R.mipmap.ic_home,
            R.mipmap.ic_favorite,
            R.mipmap.ic_settings,
            R.mipmap.ic_info
    };

    private String[] titles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        databaseHelper = new DatabaseHelper(this);

        initView();
    }

    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        titles = getResources().getStringArray(R.array.tabs);

        fragmentDrawer = (NavigationFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        fragmentDrawer.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        fragmentDrawer.setDrawerListener(this);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

    }

    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(alarmReceiver, new IntentFilter("Alarm"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(alarmReceiver);
    }

    public DatabaseHelper getDatabase() {
        return databaseHelper;
    }

    private void createViewTab(int index) {
        RelativeLayout tabMovie = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        TextView title = (TextView) tabMovie.findViewById(R.id.title);
        title.setText(titles[index]);

        ImageView icon = (ImageView) tabMovie.findViewById(R.id.icon);
        icon.setImageResource(tabIcons[index]);
        tabLayout.getTabAt(index).setCustomView(tabMovie);
    }

    private void setupTabIcons() {
        for (int i = 0; i < 4; i++) {
            createViewTab(i);
        }
        updateLike();

    }

    public int getCurrentTab() {
        return viewPager.getCurrentItem();
    }

    public void updateLike() {
        RelativeLayout tabMovie = (RelativeLayout) tabLayout.getTabAt(TAB_FAVOURITE_INDEX).getCustomView();
        TextView title = (TextView) tabMovie.findViewById(R.id.notify);
        List<Movie> movies = databaseHelper.getMovies();
        if (movies != null) {
            if (movies.size() > 0) {
                title.setVisibility(View.VISIBLE);
            } else {
                title.setVisibility(View.INVISIBLE);
            }

            title.setText(String.valueOf(movies.size()));
        }

    }


    private void setupViewPager(final ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(MovieFragment.newInstance(MovieFragment.LISTVIEW_MODE), "Movie");
        adapter.addFragment(new FavouriteFragment(), "Favourite");
        adapter.addFragment(new SettingFragment(), "Setting");
        adapter.addFragment(new AboutFragment(), "About");
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_movie:
                viewPager.setCurrentItem(TAB_MOVIE_INDEX);
                break;

            case R.id.action_favourite:
                viewPager.setCurrentItem(TAB_FAVOURITE_INDEX);
                break;
            case R.id.action_setting:
                viewPager.setCurrentItem(TAB_SETTING_INDEX);
                break;
            case R.id.action_about:
                viewPager.setCurrentItem(TAB_ABOUT_INDEX);
                break;

        }


        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onShowAllReminder() {
        //show tab setting
        TabLayout.Tab tab = tabLayout.getTabAt(TAB_SETTING_INDEX);
        tab.select();

        ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
        SettingFragment fragment = (SettingFragment) adapter.getItem(TAB_SETTING_INDEX);
        fragment.gotoReminder();

    }

    @Override
    public void onEditProfile() {
        ActionEvent actionEvent = new ActionEvent();
        actionEvent.fragmentActivity = this;
        actionEvent.tag = ActionEventConstant.PROFILE_FRAGMENT;
        Navigator.getInstance().handleSwitchFragment(actionEvent);
    }

    @Override
    public void onMovieUpdateFavourite(int movieId, boolean isFavourite) {

        //update to favourite
        ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
        FavouriteFragment fragment = (FavouriteFragment) adapter.getItem(TAB_FAVOURITE_INDEX);
        fragment.updateDataFavourite(movieId, isFavourite);

        //update to setting
        SettingFragment settingFragment = (SettingFragment) adapter.getItem(TAB_SETTING_INDEX);
        settingFragment.updateDataFavourite(movieId, isFavourite);

        updateLike();
    }

    @Override
    public void onMovieUpdateReminder(int movieId, String date) {
        fragmentDrawer.getNavigationPresent().fetchReminder();
        //update to setting
        ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
        SettingFragment settingFragment = (SettingFragment) adapter.getItem(TAB_SETTING_INDEX);
        settingFragment.updateDataReminder(movieId, date);

        //update to favourite
        FavouriteFragment favouriteFragment = (FavouriteFragment) adapter.getItem(TAB_FAVOURITE_INDEX);
        favouriteFragment.updateDataReminder(movieId, date);

    }


    @Override
    public void onFavouriteUpdateFavourite(int movieId, boolean isFavourite) {
        //update to movie
        ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
        MovieFragment movieFragment = (MovieFragment) adapter.getItem(TAB_MOVIE_INDEX);
        movieFragment.updateDataFavourite(movieId, isFavourite);

        //update to reminder
        SettingFragment settingFragment = (SettingFragment) adapter.getItem(TAB_SETTING_INDEX);
        settingFragment.updateDataFavourite(movieId, isFavourite);

        updateLike();
    }

    @Override
    public void onFavouriteUpdateReminder(int movieId, String date) {
        fragmentDrawer.getNavigationPresent().fetchReminder();

        //update to setting
        ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
        SettingFragment settingFragment = (SettingFragment) adapter.getItem(TAB_SETTING_INDEX);
        settingFragment.updateDataReminder(movieId, date);

        //update to favourite
        MovieFragment movieFragment = (MovieFragment) adapter.getItem(TAB_MOVIE_INDEX);
        movieFragment.updateDataReminder(movieId, date);

    }


    @Override
    public void onSoftChange() {
        ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
        MovieFragment fragment = (MovieFragment) adapter.getItem(TAB_MOVIE_INDEX);
        fragment.updateSoft();
    }

    @Override
    public void onFilterChange() {
        ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
        MovieFragment fragment = (MovieFragment) adapter.getItem(TAB_MOVIE_INDEX);
        fragment.updateFilter();
    }

    @Override
    public void onUpdateProfile(int profileId) {
        fragmentDrawer.getNavigationPresent().fetchProfile(profileId);
    }

    @Override
    public void onSettingDeleteReminder() {
        fragmentDrawer.getNavigationPresent().fetchReminder();
        //update to movie
        ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
        MovieFragment movieFragment = (MovieFragment) adapter.getItem(TAB_MOVIE_INDEX);
        movieFragment.deleteReminder();

        //update to favourite
        FavouriteFragment favouriteFragment = (FavouriteFragment) adapter.getItem(TAB_FAVOURITE_INDEX);
        favouriteFragment.deleteReminder();
    }

    @Override
    public void onSettingUpdateFavourite(int movieId, boolean isFavourite) {
        ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();

        MovieFragment movieFragment = (MovieFragment) adapter.getItem(TAB_MOVIE_INDEX);
        movieFragment.updateDataFavourite(movieId, isFavourite);

        FavouriteFragment favouriteFragment = (FavouriteFragment) adapter.getItem(TAB_FAVOURITE_INDEX);
        favouriteFragment.updateDataFavourite(movieId, isFavourite);

        updateLike();

    }

    @Override
    public void onSettingUpdateReminder(int movieId, String date) {
        fragmentDrawer.getNavigationPresent().fetchReminder();

        //update to favourite
        ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
        FavouriteFragment favouriteFragment = (FavouriteFragment) adapter.getItem(TAB_FAVOURITE_INDEX);
        favouriteFragment.updateDataReminder(movieId, date);

        //update to favourite
        MovieFragment movieFragment = (MovieFragment) adapter.getItem(TAB_MOVIE_INDEX);
        movieFragment.updateDataReminder(movieId, date);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

    }

    public void gotoDetailCast() {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        DetailFragment detailMovieFragment = (DetailFragment) fm.findFragmentByTag(DetailFragment.TAG);
        if (detailMovieFragment != null) {
            ft.detach(detailMovieFragment);
        }

        detailMovieFragment = new DetailFragment();

        ft.add(R.id.container_main, detailMovieFragment, DetailFragment.TAG);
        ft.addToBackStack(DetailFragment.TAG);
        ft.commit();
        fm.executePendingTransactions();
    }

    private BroadcastReceiver alarmReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            SimpleDateFormat dateFormat = new SimpleDateFormat();
            Toast.makeText(context, "Alarm Receiver: " + dateFormat.format(new Date()), Toast.LENGTH_SHORT).show();
            ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();

            //update navigation
            fragmentDrawer.getNavigationPresent().fetchReminder();

            //update to setting
            SettingFragment setting = (SettingFragment) adapter.getItem(TAB_SETTING_INDEX);
            setting.deleteReminder();

            //update movie
            MovieFragment movie = (MovieFragment) adapter.getItem(TAB_MOVIE_INDEX);
            movie.deleteReminder();

            //update favourite
            FavouriteFragment favourite = (FavouriteFragment) adapter.getItem(TAB_FAVOURITE_INDEX);
            favourite.deleteReminder();

        }
    };

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ProfileFragment profileFragment = (ProfileFragment) fm.findFragmentByTag(ProfileFragment.TAG);
        DetailFragment detailFragment = (DetailFragment) fm.findFragmentByTag(DetailFragment.TAG);
        if (profileFragment != null && (profileFragment.isVisible())) {
            profileFragment.hideKeyboard();
            fm.popBackStack();
        } else if (detailFragment != null && (detailFragment.isVisible())) {
            // profileFragment.hideKeyboard();
            fm.popBackStack();
        } else {

            if (!fragmentDrawer.isOpen()) {
                Fragment fragment = ((ViewPagerAdapter) viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                FragmentManager fragmentManager = fragment.getChildFragmentManager();
                if (fragmentManager.getBackStackEntryCount() > 0) {
                    int sizeStack = fragmentManager.getBackStackEntryCount();

                    Fragment chiFragment = fragmentManager.getFragments().get(sizeStack - 1);
                    FragmentManager childFragmentManager = chiFragment.getChildFragmentManager();

                    if (chiFragment instanceof PreferencesFragment) {
                        super.onBackPressed();
                        return;
                    }


                    if (childFragmentManager.getBackStackEntryCount() > 0) {
                        childFragmentManager.popBackStack();
                    } else {
                        fragmentManager.popBackStack();
                    }
                } else {
                    super.onBackPressed();
                }
            } else {
                fragmentDrawer.close();
            }
        }
//        for (Fragment frag : fragmentManager.getFragments()) {
//            if (frag.isVisible()) {
//                FragmentManager childFm = frag.getChildFragmentManager();
//                if (childFm.getBackStackEntryCount() > 0) {
//                    childFm.popBackStack();
//                    return;
//                }
//            }
//        }
//        super.onBackPressed();
    }
}

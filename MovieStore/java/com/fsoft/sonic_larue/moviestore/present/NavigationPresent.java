package com.fsoft.sonic_larue.moviestore.present;

import android.content.Context;
import android.util.Log;

import com.fsoft.sonic_larue.moviestore.entity.Profile;
import com.fsoft.sonic_larue.moviestore.entity.Reminder;
import com.fsoft.sonic_larue.moviestore.listener.OnGetReminderFinishListener;
import com.fsoft.sonic_larue.moviestore.model.INavigationModel;
import com.fsoft.sonic_larue.moviestore.model.NavigationModel;
import com.fsoft.sonic_larue.moviestore.view.NavigationView;

import java.util.List;

/**
 * Created by DungHT8 on 2015/12/08.
 */
public class NavigationPresent implements INavigationPresent, OnGetReminderFinishListener {
    private Context context;
    private NavigationView navigationView;
    private INavigationModel navigationModel;

    public NavigationPresent(NavigationView navigationView, Context context) {
        this.context = context;
        this.navigationView = navigationView;
        navigationModel = new NavigationModel(context);
    }

    @Override
    public void fetchReminder() {
        navigationModel.fetchReminder(this);

    }

    @Override
    public void fetchProfile(int profileId) {
        navigationModel.fetchProfile(this, profileId);

    }

    @Override
    public void onSuccess(List<Reminder> movies) {
        navigationView.setDataReminder(movies);
    }

    @Override
    public void onError() {

    }

    @Override
    public void onSuccessGetProfile(Profile profile) {
    navigationView.setDataProfile(profile);
    }
}

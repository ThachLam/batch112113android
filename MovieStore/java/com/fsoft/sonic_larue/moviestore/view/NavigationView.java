package com.fsoft.sonic_larue.moviestore.view;

import com.fsoft.sonic_larue.moviestore.entity.Movie;
import com.fsoft.sonic_larue.moviestore.entity.Profile;
import com.fsoft.sonic_larue.moviestore.entity.Reminder;

import java.util.List;

/**
 * Created by DungHT8 on 2015/12/08.
 */
public interface NavigationView {
    void setDataReminder(List<Reminder> reminders);
    void setDataProfile(Profile profile);
}

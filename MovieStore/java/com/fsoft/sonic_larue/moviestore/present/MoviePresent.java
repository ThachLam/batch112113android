package com.fsoft.sonic_larue.moviestore.present;

import com.fsoft.sonic_larue.moviestore.entity.Movie;
import com.fsoft.sonic_larue.moviestore.model.IMovieModel;
import com.fsoft.sonic_larue.moviestore.model.MovieModel;
import com.fsoft.sonic_larue.moviestore.listener.OnGetMoviesFinishListener;
import com.fsoft.sonic_larue.moviestore.entity.Setting;
import com.fsoft.sonic_larue.moviestore.view.MovieView;

import java.util.List;

/**
 * Created by DungHT8 on 2015/12/05.
 */
public class MoviePresent implements IMoviePresent, OnGetMoviesFinishListener {
    private MovieView movieView;
    private IMovieModel movieModel;

    public MoviePresent(MovieView movieView) {
        this.movieView = movieView;
        this.movieModel = new MovieModel();
    }

    @Override
    public void fetchMovie(final int numberPage, Setting setting) {
        this.movieView.showProgress();
        this.movieModel.fetchMovie(this, numberPage, setting);
    }

    @Override
    public void onSuccess(List<Movie> movies) {
        movieView.hideProgress();
        movieView.setDataMovies(movies);
    }

    @Override
    public void onError() {
        movieView.hideProgress();
    }
}

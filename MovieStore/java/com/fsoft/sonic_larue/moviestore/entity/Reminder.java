package com.fsoft.sonic_larue.moviestore.entity;

/**
 * Created by DungHT8 on 2015/11/25.
 */
public class Reminder {
    private String title;
    private String date;
    private String posterPath;
    private float rate;
    private String year;
    private int movieId;
    private int id;

    public static final int DRAWER = 100;
    public static final int FRAGMENT = 101;

    public  static final String ID = "id";
    public  static final String TITLE = "title";
    public  static final String YEAR = "year";
    public  static final String RATE = "rate";
    public  static final String POSTER_PATH = "poster_path";
    public  static final String DATE = "date";
    public  static final String MOVIE_ID = "movie_id";

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }
}

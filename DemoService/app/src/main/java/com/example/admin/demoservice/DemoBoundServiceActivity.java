package com.example.admin.demoservice;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class DemoBoundServiceActivity extends AppCompatActivity {

    static final String TAG = DemoBoundServiceActivity.class.getSimpleName();
    BoundIntentService mService;
    boolean mBound = false;

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            BoundIntentService.LocalBinder binder = (BoundIntentService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo_bound_service);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent i = new Intent(this, BoundIntentService.class);
        bindService(i, mConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    public void start(View view) {
        Intent i = new Intent(this, BoundIntentService.class);
        startService(i);
    }

    public void stop(View view) {
//        if (mBound) {
//            unbindService(mConnection);
//            mBound = false;
//        }
//        Intent i = new Intent(this, BoundService.class);
//        stopService(i);
    }

    public void setValueOfK(View view) {
        EditText txtK = (EditText) findViewById(R.id.txtKValue);
        mService.setK(Integer.parseInt(txtK.getText().toString()));
    }

    public void getKValue(View view) {
        TextView tvK = (TextView) findViewById(R.id.tvK);
        tvK.setText(mService.getK() + "");
    }
}

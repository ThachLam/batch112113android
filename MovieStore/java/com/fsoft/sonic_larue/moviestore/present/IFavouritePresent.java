package com.fsoft.sonic_larue.moviestore.present;

/**
 * Created by DungHT8 on 2015/12/08.
 */
public interface IFavouritePresent {
    void fetchData(String filter);
    void searchData(String filter);
}

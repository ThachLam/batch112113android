package com.example.admin.demorecyclerview;

/**
 * Created by Admin on 8/19/2016.
 */
public class Movie {

    String name;
    String description;

    public Movie(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

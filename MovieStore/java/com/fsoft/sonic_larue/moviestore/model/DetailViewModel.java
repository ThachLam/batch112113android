package com.fsoft.sonic_larue.moviestore.model;

import android.content.Context;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fsoft.sonic_larue.moviestore.alarm.ScheduleManager;
import com.fsoft.sonic_larue.moviestore.application.MyApplication;
import com.fsoft.sonic_larue.moviestore.database.DatabaseHelper;
import com.fsoft.sonic_larue.moviestore.entity.Cast;
import com.fsoft.sonic_larue.moviestore.entity.Movie;
import com.fsoft.sonic_larue.moviestore.entity.Reminder;
import com.fsoft.sonic_larue.moviestore.listener.OnGetCastFinishListener;
import com.fsoft.sonic_larue.moviestore.listener.OnGetMovieFinishListener;
import com.fsoft.sonic_larue.moviestore.listener.OnGetDetailFinishListener;
import com.fsoft.sonic_larue.moviestore.util.GlobalVariable;
import com.fsoft.sonic_larue.moviestore.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by DungHT8 on 2015/12/08.
 */
public class DetailViewModel implements IDetailMovieModel {
    private Context context;
    private DatabaseHelper databaseHelper;

    public DetailViewModel(Context context) {
        this.context = context;
        this.databaseHelper = new DatabaseHelper(context);

    }

    @Override
    public void fetchCast(final OnGetDetailFinishListener onGetDetailFinishListener, int movieID) {
        String url = GlobalVariable.getApiCastCrewList(movieID);
        final List<Cast> casts = new ArrayList<>();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonCast) {
                        try {
                            JSONArray jsonArray = jsonCast.getJSONArray("cast");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                Cast cast = new Cast();
                                cast.setCastId(object.getInt(Cast.CAST_ID));
                                cast.setName(object.getString(Cast.NAME));
                                cast.setProfilePath(object.getString(Cast.PROFILE_PATH));
                                cast.setCreditId(object.getString(Cast.CREDIT_ID));
                                casts.add(cast);
                            }
                            onGetDetailFinishListener.onSuccessCasts(casts);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            onGetDetailFinishListener.onErrorCast();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // hide the progress dialog
                onGetDetailFinishListener.onErrorCast();
            }
        });
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }


    @Override
    public void fetchMovie(OnGetDetailFinishListener onGetDetailFinishListener, Bundle bundle) {
        Movie movie;
        if (bundle != null) {
            movie = bundle.getParcelable("object");
            int movieId = bundle.getInt("movieId");
            if (movie != null) {
                onGetDetailFinishListener.onSuccessMovie(movie);
            } else {
                fetchMovie(onGetDetailFinishListener, movieId);
            }
        } else {
            onGetDetailFinishListener.onErrorMovie();
        }
    }

    @Override
    public void updateReminder(Movie currentMovie, Calendar calendar, OnGetDetailFinishListener onUpdateFinishListener) {
        Reminder reminder = new Reminder();
        Date date = calendar.getTime();
        date.setTime(date.getTime());
        SimpleDateFormat format = new SimpleDateFormat(Util.DATE_FORMAT);
        String formattedDate = format.format(date);

        if (!databaseHelper.isReminder(currentMovie.getId())) {
            reminder.setRate(currentMovie.getRating());
            reminder.setPosterPath(currentMovie.getPoster());
            reminder.setTitle(currentMovie.getTitle());
            reminder.setDate(formattedDate);
            reminder.setMovieId(currentMovie.getId());

            String dates[] = currentMovie.getReleaseDate().split("-");
            reminder.setYear(dates[0]);

            //insert database
            long insert = databaseHelper.insertReminder(reminder);
            if (insert != -1) {
                onUpdateFinishListener.onSuccessUpdate(reminder.getDate());
            } else {
                onUpdateFinishListener.onErrorUpdate();
            }

        } else {
            //update database
            reminder = databaseHelper.getReminder(currentMovie.getId());
            reminder.setDate(formattedDate);

            long update = databaseHelper.updateReminder(reminder);
            if (update != -1) {
                onUpdateFinishListener.onSuccessUpdate(reminder.getDate());
            } else {
                onUpdateFinishListener.onErrorUpdate();
            }
        }
        //set Alarm
        ScheduleManager.setAlarm(context, calendar.getTimeInMillis(), reminder.getMovieId());
    }

    @Override
    public void fetchMovie(final OnGetDetailFinishListener onGetDetailFinishListener, final int movieID) {
        String url = GlobalVariable.getApiMovieDetails(movieID);
        final Movie currentMovie = new Movie();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonMovie) {
                        try {
                            currentMovie.setId(movieID);
                            currentMovie.setAdult(jsonMovie.getBoolean(Movie.ADULT));
                            currentMovie.setPoster(jsonMovie.getString(Movie.POSTER));
                            currentMovie.setOverview(jsonMovie.getString(Movie.OVERVIEW));
                            currentMovie.setReleaseDate(jsonMovie.getString(Movie.RELEASE_DATE));
                            currentMovie.setRating((float) jsonMovie.getDouble(Movie.VOTE_AVERAGE));
                            currentMovie.setTitle(jsonMovie.getString(Movie.TITLE));
                            onGetDetailFinishListener.onSuccessMovie(currentMovie);
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                            onGetDetailFinishListener.onErrorMovie();
                        }
                    }
                }, new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {
                onGetDetailFinishListener.onErrorMovie();
            }
        }

        );
        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


}

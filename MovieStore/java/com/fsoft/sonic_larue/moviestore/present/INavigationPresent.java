package com.fsoft.sonic_larue.moviestore.present;

/**
 * Created by DungHT8 on 2015/12/08.
 */
public interface INavigationPresent {
    void fetchReminder();
    void fetchProfile(int profileId);
}

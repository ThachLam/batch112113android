package com.fsoft.sonic_larue.moviestore.listener;

import com.fsoft.sonic_larue.moviestore.entity.Cast;
import com.fsoft.sonic_larue.moviestore.entity.Reminder;

import java.util.List;

/**
 * Created by DungHT8 on 2015/12/05.
 */
public interface OnGetCastFinishListener {
    void onSuccess(List<Cast> casts);
    void onError();
}

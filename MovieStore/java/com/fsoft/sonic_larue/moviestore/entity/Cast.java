package com.fsoft.sonic_larue.moviestore.entity;

/**
 * Created by DungHT8 on 2015/11/25.
 */
public class Cast {
    public static final String CAST_ID = "cast_id";
    public static final String CHARACTER = "character";
    public static final String NAME = "name";
    public static final String PROFILE_PATH = "profile_path";
    public static final String CREDIT_ID = "credit_id";
    private int castId;
    private String character;
    private String name;
    private String profilePath;
    private String creditId;

    public int getCastId() {
        return castId;
    }

    public void setCastId(int castId) {
        this.castId = castId;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePath() {
        return profilePath;
    }

    public void setProfilePath(String profilePath) {
        this.profilePath = profilePath;
    }

    public String getCreditId() {
        return creditId;
    }

    public void setCreditId(String creditId) {
        this.creditId = creditId;
    }
}

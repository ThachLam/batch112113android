package com.fsoft.sonic_larue.moviestore.present;

import android.os.Bundle;

import com.fsoft.sonic_larue.moviestore.entity.Movie;

import java.util.Calendar;

/**
 * Created by DungHT8 on 2015/12/08.
 */
public interface IDetailViewPresent {
    void fetchCast(int movieID);
    void fetchMovie(Bundle bundle);
    void updateReminder(Movie movie, Calendar calendar);
}
